"""Natural orbital occupation module."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

from collections.abc import Mapping
from typing import NamedTuple

import numpy as np


class NatOrbOcc(NamedTuple):
    """Data structure for natural orbital occupations."""

    num_irreps: int
    strong_nat_occ: dict[int, np.ndarray]
    weak_nat_occ: dict[int, np.ndarray]
    strong_nat_occ_nosym: np.ndarray
    strong_nat_occ_nosym2sym_idx: np.ndarray
    weak_nat_occ_nosym: np.ndarray
    weak_nat_occ_nosym2sym_idx: np.ndarray


class CAS(NamedTuple):
    """Data structure for complete active space specification."""

    active_electrons: int
    active_orbitals: int | list[int]
    inactive_orbitals: int | list[int]


def trim_natural_occupations(natural_occupations: Mapping[int, np.ndarray],
                             strong_threshold: float = 1.995,
                             weak_threshold: float = 0.002) -> str:
    """Print relevant natural occupations in a nice format.

    Args:
        natural_occupations: Natural occupation numbers.
        strong_threshold: Strongly occupied natural orbitals with occupations
            above this threshold will not be printed.
        weak_threshold: Weakly occupied natural orbitals with occupations below
            this threshold will not be printed.

    Returns:
        String to be printed or saved to file.
    """
    nat_occ_sorting = sort_natural_occupations(natural_occupations)
    out_string = ''
    for key in range(1, nat_occ_sorting.num_irreps + 1):
        if key != 1:
            out_string += '\n\n'
        out_string += f'Symmetry: {key}\n'
        orbital_counter = 0
        for nat_occ in nat_occ_sorting.strong_nat_occ[key][::-1]:
            if nat_occ > strong_threshold:
                continue
            if orbital_counter % 5 == 0:
                out_string += '\n'
            orbital_counter += 1
            out_string += f'{nat_occ:.4f}   '
        for nat_occ in nat_occ_sorting.weak_nat_occ[key]:
            if nat_occ < weak_threshold:
                continue
            if orbital_counter % 5 == 0:
                out_string += '\n'
            orbital_counter += 1
            out_string += f'{nat_occ:.4f}   '
    return out_string


def scan_occupations(natural_occupations: Mapping[int, np.ndarray],
                     strong_threshold: float = 1.995,
                     weak_threshold: float = 0.002,
                     max_orbitals: int = 14) -> str:
    """Determine thresholds for picking active space.

    Will not print orbitals outside the bounds of "strong_threshold" and
    "weak_threshold".

    Args:
        natural_occupations: Natural occupation numbers.
        strong_threshold: Strongly occupied natural orbitals with occupations
            above this threshold will not be printed.
        weak_threshold: Weakly occupied natural orbitals with occupations below
            this threshold will not be printed.
        max_orbitals: Maximum number of strongly and weakly occupied orbitals
            to print.

    Returns:
        String to be printed or saved to file.
    """
    nat_occ_sorting = sort_natural_occupations(natural_occupations)
    out_string = ''
    out_string += 'Strongly occupied natural orbitals                      ' \
                  'Weakly occupied natural orbitals\n'
    out_string += 'Symmetry   Occupation   Change in occ.   Diff. to 2' \
                  '     Symmetry   Occupation   Change in occ.\n'
    orbital_counter = 0
    strong_previous_occupation = nat_occ_sorting.strong_nat_occ_nosym[0]
    weak_previous_occupation = nat_occ_sorting.weak_nat_occ_nosym[0]
    # Padding of the occupations is needed in case there are more relevant virtual orbitals than active orbitals.
    tmp_strong_nat_occ_nosym = nat_occ_sorting.strong_nat_occ_nosym.copy()
    tmp_strong_nat_occ_nosym2sym_idx = nat_occ_sorting.strong_nat_occ_nosym2sym_idx.copy()
    tmp_weak_nat_occ_nosym = nat_occ_sorting.weak_nat_occ_nosym.copy()
    tmp_weak_nat_occ_nosym2sym_idx = nat_occ_sorting.weak_nat_occ_nosym2sym_idx.copy()
    if len(tmp_weak_nat_occ_nosym) > len(tmp_strong_nat_occ_nosym):
        npad = len(tmp_weak_nat_occ_nosym)
        tmp_strong_nat_occ_nosym = np.pad(tmp_strong_nat_occ_nosym,
                                          pad_width=npad,
                                          mode='constant',
                                          constant_values=10)[npad:]
        tmp_strong_nat_occ_nosym2sym_idx = np.pad(tmp_strong_nat_occ_nosym2sym_idx,
                                                  pad_width=npad,
                                                  mode='constant',
                                                  constant_values=10)[npad:]
    elif len(tmp_weak_nat_occ_nosym) < len(tmp_strong_nat_occ_nosym):
        npad = len(tmp_strong_nat_occ_nosym)
        tmp_weak_nat_occ_nosym = np.pad(tmp_weak_nat_occ_nosym,
                                        pad_width=npad,
                                        mode='constant',
                                        constant_values=-10)[npad:]
        tmp_weak_nat_occ_nosym2sym_idx = np.pad(tmp_weak_nat_occ_nosym2sym_idx,
                                                pad_width=npad,
                                                mode='constant',
                                                constant_values=-10)[npad:]
    for strong_occupation, strong_symmetry, weak_occupation, weak_symmetry in zip(
            tmp_strong_nat_occ_nosym, tmp_strong_nat_occ_nosym2sym_idx, tmp_weak_nat_occ_nosym,
            tmp_weak_nat_occ_nosym2sym_idx):
        if strong_occupation > strong_threshold and weak_occupation < weak_threshold:
            break
        if strong_occupation <= strong_threshold:
            out_string += f'{strong_symmetry:4}{strong_occupation:16.4f}' \
                          f'{strong_occupation-strong_previous_occupation:14.4f}{2-strong_occupation:14.4f}   '
            strong_previous_occupation = strong_occupation
        else:
            out_string += '---------------------------------------------------'
        if weak_occupation >= weak_threshold:
            out_string += f'{weak_symmetry:10}{weak_occupation:16.4f}' \
                          f'{weak_previous_occupation-weak_occupation:14.4f}'
            weak_previous_occupation = weak_occupation
        else:
            out_string += '--------------------------------------------'
        orbital_counter += 1
        out_string += '\n'
        if orbital_counter == max_orbitals:
            break
    return out_string


def pick_cas_by_thresholds(natural_occupations: Mapping[int, np.ndarray], max_strong_occupation: float,
                           min_weak_occupation: float) -> CAS:
    """Get parameters needed for a CAS calculation based on simple thresholds.

    Args:
        natural_occupations: Natural occupation numbers.
        max_strong_occupation: Maximum occupation number of strongly occupied
            natural orbitals to be included in CAS.
        min_weak_occupation: Minimum occupation number of weakly occupied
            natural orbitals to be included in CAS.

    Returns:
        Number of active electrons, CAS, and number of inactive orbitals.
    """
    nat_occ_sorting = sort_natural_occupations(natural_occupations)
    electrons = 0
    cas = [0] * nat_occ_sorting.num_irreps
    inactive = [0] * nat_occ_sorting.num_irreps
    for occupation, symmetry in zip(nat_occ_sorting.strong_nat_occ_nosym,
                                    nat_occ_sorting.strong_nat_occ_nosym2sym_idx):
        if occupation > max_strong_occupation:
            inactive[symmetry - 1] += 1
        else:
            cas[symmetry - 1] += 1
            electrons += 2
    for occupation, symmetry in zip(nat_occ_sorting.weak_nat_occ_nosym,
                                    nat_occ_sorting.weak_nat_occ_nosym2sym_idx):
        if occupation > min_weak_occupation:
            cas[symmetry - 1] += 1
        else:
            break
    if nat_occ_sorting.num_irreps == 1:
        return CAS(electrons, cas[0], inactive[0])
    return CAS(electrons, cas, inactive)


def sort_natural_occupations(natural_occupations: np.ndarray | Mapping[int, np.ndarray]) -> NatOrbOcc:
    """Sort natural orbital occupation numbers.

    Args:
        natural_occupations: Natural occupation numbers.

    Returns:
        Sorted natural orbital occupation numbers.
    """
    if isinstance(natural_occupations, np.ndarray):
        natural_occupations = {1: natural_occupations}
    if not isinstance(natural_occupations, dict):
        raise TypeError('natural_occupations must either be a NumPy array or a dict of NumPy arrays.')
    num_irreps = max(natural_occupations.keys())
    # Strongly occupied.
    strong_nat_occ = {}
    # Weakly occupied.
    weak_nat_occ = {}
    for sym_num, nat_occs in natural_occupations.items():
        # Strong occupations have smallest numbers first.
        strong_nat_occ[sym_num] = np.sort(nat_occs[nat_occs >= 1.0])
        # Weak occupations have largest numbers first.
        weak_nat_occ[sym_num] = np.sort(nat_occs[nat_occs < 1.0])[::-1]
    # Make np.ndarray of the natural occupations without symmetry, and an array keeping track of their symmetry.
    strong_nat_occ_nosym: list[float] | np.ndarray = []
    strong_nat_occ_nosym2sym_idx: list[int] | np.ndarray = []
    for sym_num, nat_occs in strong_nat_occ.items():
        strong_nat_occ_nosym = strong_nat_occ_nosym + nat_occs.tolist()
        strong_nat_occ_nosym2sym_idx = strong_nat_occ_nosym2sym_idx + [sym_num] * len(nat_occs)
    weak_nat_occ_nosym: list[float] | np.ndarray = []
    weak_nat_occ_nosym2sym_idx: list[float] | np.ndarray = []
    for sym_num, nat_occs in weak_nat_occ.items():
        weak_nat_occ_nosym = weak_nat_occ_nosym + nat_occs.tolist()
        weak_nat_occ_nosym2sym_idx = weak_nat_occ_nosym2sym_idx + [sym_num] * len(nat_occs)
    strong_nat_occ_nosym = np.array(strong_nat_occ_nosym)
    strong_nat_occ_nosym2sym_idx = np.array(strong_nat_occ_nosym2sym_idx)
    weak_nat_occ_nosym = np.array(weak_nat_occ_nosym)
    weak_nat_occ_nosym2sym_idx = np.array(weak_nat_occ_nosym2sym_idx)
    # Make sure the arrays are correctly sorted.
    idx = np.argsort(strong_nat_occ_nosym)
    strong_nat_occ_nosym = strong_nat_occ_nosym[idx]
    strong_nat_occ_nosym2sym_idx = strong_nat_occ_nosym2sym_idx[idx]
    idx = np.argsort(weak_nat_occ_nosym)[::-1]
    weak_nat_occ_nosym = weak_nat_occ_nosym[idx]
    weak_nat_occ_nosym2sym_idx = weak_nat_occ_nosym2sym_idx[idx]
    return NatOrbOcc(num_irreps, strong_nat_occ, weak_nat_occ, strong_nat_occ_nosym, strong_nat_occ_nosym2sym_idx,
                     weak_nat_occ_nosym, weak_nat_occ_nosym2sym_idx)
