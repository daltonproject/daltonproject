"""Numerical derivatives"""
#  Dalton Project: A Python platform for molecular- and electronic-structure
#  simulations of complex systems
#
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

from collections.abc import Callable, Sequence
from typing import NamedTuple

import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly
from qcelemental import PhysicalConstantsContext

constants = PhysicalConstantsContext('CODATA2018')


class StoreData(NamedTuple):
    """Data structure for manipulating data."""

    x: np.ndarray
    y: np.ndarray
    coefs: np.ndarray
    label_x_axis: str
    label_y_axis: str
    title_plot: str


class PlotData(NamedTuple):
    """Data structure for plotting."""

    x: np.ndarray
    y: np.ndarray
    coefs: np.ndarray
    label_x_axis: str
    label_y_axis: str
    title_plot: str
    axis: np.ndarray
    plot_idx: int


def linear_cfcs(hessians: np.ndarray, dq: np.ndarray, order: int) -> np.ndarray:
    """Calculate cubic force constants numerically using a linear 3/5 point stencil

    Args:
        hessians: A 3D matrix of Hessians at displaced geometries.
        dq: Step size along normal coordinate.
        order: Point number of stencil.

    Returns:
        k_ijk: Cubic force constants.
    """
    dim = hessians.shape[0] // (order-1)
    count = order - 1
    k_ijk = np.zeros((dim, dim, dim))
    indices = np.indices((dim, dim, dim))
    i_idx, j_idx, k_idx = indices[0], indices[1], indices[2]
    if order == 3:
        k_ijk = (hessians[i_idx * count + 1, j_idx, k_idx]
                 - hessians[i_idx * count, j_idx, k_idx]) / (2 * dq[i_idx]) + \
                (hessians[j_idx * count + 1, k_idx, i_idx]
                 - hessians[j_idx * count, k_idx, i_idx]) / (2 * dq[j_idx]) + \
                (hessians[k_idx * count + 1, i_idx, j_idx]
                 - hessians[k_idx * count, i_idx, j_idx]) / (2 * dq[k_idx])
    elif order == 5:
        k_ijk = (hessians[i_idx * count, j_idx, k_idx]
                 - 8 * hessians[i_idx * count + 1, j_idx, k_idx]
                 + 8 * hessians[i_idx * count + 2, j_idx, k_idx]
                 - hessians[i_idx * count + 3, j_idx, k_idx]) / (12 * dq[i_idx]) + \
                (hessians[j_idx * count, k_idx, i_idx]
                 - 8 * hessians[j_idx * count + 1, k_idx, i_idx] + 8
                 * hessians[j_idx * count + 2, k_idx, i_idx]
                 - hessians[j_idx * count + 3, k_idx, i_idx]) / (12 * dq[j_idx]) + \
                (hessians[k_idx * count, i_idx, j_idx]
                 - 8 * hessians[k_idx * count + 1, i_idx, j_idx]
                 + 8 * hessians[k_idx * count + 2, i_idx, j_idx]
                 - hessians[k_idx * count + 3, i_idx, j_idx]) / (12 * dq[k_idx])
    k_ijk *= 1 / 3
    return k_ijk


def polynomial_cfcs(hessians: np.ndarray, hessian_eq: np.ndarray, dq: np.ndarray, order: int) -> np.ndarray:
    """Calculate cubic force constants numerically using a polynomial fit.

    Args:
        hessian: 3D Matrix of Hessian displaced geometries.
        hessian_eq: Hessian at the equilibrium geometry.
        dq: Step size.
        order: Order of polynomial.

    Returns:
        k_ijk: Cubic force constants.
    """
    dim = hessians.shape[0] // 4
    k_ijk = np.zeros((dim, dim, dim))
    for i, j, k in np.ndindex((dim, dim, dim)):
        val1 = poly_derivative(dq, hessians, hessian_eq, order, i, j, k)
        val2 = poly_derivative(dq, hessians, hessian_eq, order, j, k, i)
        val3 = poly_derivative(dq, hessians, hessian_eq, order, k, i, j)
        k_ijk[i, j, k] = 1 / 3 * (val1+val2+val3)
    return k_ijk


def poly_derivative(dq: np.ndarray, hessians: np.ndarray, hessian_eq: np.ndarray, order: int, i: int, j: int,
                    k: int) -> np.ndarray:
    """Derivative of a polynomial fit.

    Args:
        dq: Step size.
        hessians: 3D Matrix of Hessians at displaced geometries.
        hessian_eq: Hessian at equilibrium geometry.
        i: Atom index.
        j: Atom index.
        k: Atom index.

    Returns:
        val: Value of polynomial derivived at x=0.
    """
    x = np.array([-2 * dq[i], -dq[i], 0, dq[i], 2 * dq[i]])
    y = np.array([
        hessians[i * 4, j, k], hessians[i*4 + 1, j, k], hessian_eq[i, i], hessians[i*4 + 2, j, k],
        hessians[i*4 + 3, j, k]
    ])
    weights = np.array([1, 2, 4, 2, 1])
    coefs1 = poly.polyfit(x, y, order, w=weights)
    dcoefs1 = poly.polyder(coefs1)
    val = poly.polyval(0, dcoefs1)  # Evaluate derivative at x = 0
    return val


def linear_property(displaced_prop_vals: np.ndarray, eq_vals: np.ndarray, h: float,
                    points: int) -> tuple[np.ndarray, np.ndarray]:
    """Numerical derivatives of a property using either a 3 and 5 point stencil.

    Args:
        displaced_prop_vals: A 2D matrix of displaced property values.
        eq_vals: Property values at equilibrium geometry.
        h: Step size.
        points: Number of points in stencil.

    Returns:
        first_derivative_mat: First derivative of property.
        second_derivative_mat: Second derivative of property.
    """
    dim1 = displaced_prop_vals.shape[0]
    dim2 = eq_vals.shape[0]
    first_derivative_mat = np.zeros(dim1 * dim2)
    second_derivative_mat = np.zeros(dim1 * dim2)
    for a in range(dim1):
        for b in range(dim2):
            diff = 0
            diff2 = 0
            if points == 3:
                diff += ((displaced_prop_vals[a, (b + dim2)] - displaced_prop_vals[a, b]) / (2*h))
                diff2 += ((displaced_prop_vals[a, (b + dim2)] + displaced_prop_vals[a, b] - (2 * eq_vals[b]))) / \
                         (h**2)
            elif points == 5:
                diff += ((displaced_prop_vals[a, b] - 8 * displaced_prop_vals[a, b + dim2]
                          + 8 * displaced_prop_vals[a, b + 2*dim2] - displaced_prop_vals[a, b + 3*dim2]) / (12*h))
                diff2 += ((-displaced_prop_vals[a, b] + 16 * displaced_prop_vals[a, b + dim2] - 30 * eq_vals[b]
                           + 16 * displaced_prop_vals[a, b + 2*dim2] - displaced_prop_vals[a, b + 3*dim2])) / \
                         (12 * h**2)
            first_derivative_mat[a*dim2 + b] = diff
            second_derivative_mat[a*dim2 + b] = diff2
    return first_derivative_mat, second_derivative_mat


def polynomial_property(displaced_prop_vals: np.ndarray,
                        eq_vals: np.ndarray,
                        h: float,
                        order: int,
                        max_deg_derivative: int,
                        plot: bool,
                        labels: Sequence[str | Sequence[str]] | None = None) -> np.ndarray:
    """Numerical derivative of a property using a polynomial fit.

    Args:
        displaced_prop_vals: A 2D matrix of displaced property values.
        eq_vals: Property values at equilibrium geometry.
        h: Step size.
        order: Order of polynomial.
        max_deg_derivative: Maximum degree of derivative needed.
        plot: Plot the fitting results.
        labels: List of labels for graph.

    Returns:
        dcoefs_mat: Derivative(s) of the coefficients in matrix form.
    """

    dim1 = displaced_prop_vals.shape[0]
    dim2 = eq_vals.shape[0]
    dcoefs_mat = np.zeros((dim1 * dim2, order, max_deg_derivative))
    x = np.array([-2 * h, -h, 0, h, 2 * h])
    plot_data: list[StoreData] = []
    for a in range(dim1):
        for b in range(dim2):
            y = np.array([
                displaced_prop_vals[a, b], displaced_prop_vals[a, b + dim2], eq_vals[b],
                displaced_prop_vals[a, b + 2*dim2], displaced_prop_vals[a, b + 3*dim2]
            ])
            weights = np.array([1, 2, 4, 2, 1])
            coefs = poly.polyfit(x, y, order, w=weights)
            for i in range(max_deg_derivative):
                dcoefs = poly.polyder(coefs,
                                      i + 1)  # take derivatives with each column being a order of derivative
                dcoefs_mat[a*dim2 + b, :len(dcoefs), i] = dcoefs
            if plot:
                if labels is None:
                    raise ValueError('Please provide labels for the plot.')
                plotting_data = manipulate_data(x, y, coefs, labels, a, b)
                plot_data.append(plotting_data)
    if plot:
        call_plotfig(plot_data)
    return dcoefs_mat


def manipulate_data(x: np.ndarray, y: np.ndarray, coefs: np.ndarray, labels: Sequence[str | Sequence[str]],
                    outer_loop_idx: int, inner_loop_idx: int) -> StoreData:
    """Determine appropriate labels and store data for plotting graphs.

    Args:
        x: x-axis data.
        y: y-axis data.
        coefs: Coefficients of polynomial fit.
        labels: Sequence of labels for graph.
        outer_loop_idx: Index of outer loop.
        inner_loop_idx: Index of inner loop.

    Returns:
        plot_data: Instance containing data to be plotted.
    """
    property_label = labels[0]
    if property_label in ['DSO Corr.', 'PSO Corr.', 'SD Corr.', 'FC Corr.']:
        plot_data = StoreData(x=x,
                              y=y,
                              coefs=coefs,
                              label_x_axis='Displacement',
                              label_y_axis=f'{property_label} mode{outer_loop_idx+1} {labels[1][inner_loop_idx]}',
                              title_plot=f'{property_label}')
    elif property_label in ['Iso. Shieldings', 'Hyperfine Couplings']:
        plot_data = StoreData(x=x,
                              y=y,
                              coefs=coefs,
                              label_x_axis='Displacement',
                              label_y_axis=f'{property_label} mode{outer_loop_idx+1} {labels[1][inner_loop_idx]}',
                              title_plot=f'{property_label}')
    elif property_label in ['Polarizability', 'Opt. Rotation']:
        plot_data = StoreData(x=x,
                              y=y,
                              coefs=coefs,
                              label_x_axis='Displacement',
                              label_y_axis=f'{property_label}',
                              title_plot=f'{property_label} at {labels[1]}')
    return plot_data


def call_plotfig(plot_data: Sequence[StoreData]) -> None:
    """Call plotfig function to plot graphs.

    Args:
        plot_data: List of StoreData instances.
    """
    if len(plot_data) % 9 == 0:
        page_num = len(plot_data) // 9
    else:  # need to add one more page for the remaining plots
        page_num = len(plot_data) // 9 + 1
    for num in range(page_num):
        figure, axis = plt.subplots(3, 3, figsize=(10, 10))
        for plot_idx, data in enumerate(plot_data[(num) * 9:(num+1) * 9]):
            plotting = PlotData(data.x, data.y, data.coefs, data.label_x_axis, data.label_y_axis, data.title_plot,
                                axis, plot_idx)
            axis = plotfig(plotting)
        figure.tight_layout()
        save_fig_label = data.title_plot.replace('.', ' ').replace(' ', '_').lower()
        figure.savefig(f'{save_fig_label}_plot_{num+1}.png')
        figure.clf()


def plotfig(plotting: PlotData) -> np.ndarray:
    """Plot the fitting results.

    Args:
        plotting: Single PlotData instance to be plotted.

    Returns:
        axis: Axis for plot.
    """
    f: Callable[[float], float] = poly.Polynomial(plotting.coefs)
    x_new = np.linspace(plotting.x[0], plotting.x[-1], num=len(plotting.x) * 10)
    y_new = f(x_new)
    (o, n) = divmod(plotting.plot_idx, 3)
    plotting.axis[o, n].plot(plotting.x, plotting.y, 'o', x_new, y_new)
    plotting.axis[o, n].set_xlabel(plotting.label_x_axis)
    plotting.axis[o, n].set_ylabel(plotting.label_y_axis)
    plotting.axis[o, n].set_title(plotting.title_plot)
    plotting.axis[o, n].ticklabel_format(useOffset=False)
    return plotting.axis
