"""Dalton molecule file reader module."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

from typing import Any

import numpy as np
from qcelemental import PhysicalConstantsContext, periodictable

from .basis import Basis
from .molecule import Molecule

constants = PhysicalConstantsContext('CODATA2018')


def mol_reader(filename: str) -> tuple[Molecule, Basis]:
    """Read Dalton molecule input file.

    Args:
        filename: Name of Dalton molecule input file containing atoms and coordinates.

    Returns:
        Molecule and Basis objects.
    """
    with open(f'{filename}', 'r') as mol_file:
        filelines = mol_file.read().split('\n')
    with_generator = False
    with_atombasis = False
    lines = iter(filelines)
    line = next(lines)
    basis_set = {}
    if line.lower() == 'atombasis':
        with_atombasis = True
    elif line.lower() == 'basis':
        atom_basis = next(lines)
        basis_set['without_atombasis'] = atom_basis
    next(lines)
    next(lines)
    line4 = next(lines)
    old_format = '=' not in line4
    if old_format:
        # old molecule format
        # cartesian, # line4[0].lower() == 'c',
        atomtypes = int(line4[1:5])
        molecule_charge = int(float(line4[5:8].strip() or 0))
        num_generators = int(line4[8:10])
        generator_symbols = line4[10:20].split()
        coordinate_unit = 'bohr' if line4[20:21] == ' ' else 'angstrom'
        # threshold  = float(line4[21: 31])
        with_generator = num_generators > 0
    else:
        options = line4.split()
        molecule_charge = 0
        coordinate_unit = 'bohr'
        for idx, option in enumerate(options):
            if 'atomtypes=' in option.lower():
                atomtypes = int(float(option.split('=')[1]))
            elif 'charge=' in option.lower():
                molecule_charge = int(float(option.split('=')[1]))
            elif 'angstrom' in option.lower():
                coordinate_unit = 'angstrom'
            elif 'generators=' in option.lower():
                num_generators = int(float(option.split('=')[1]))
                generator_symbols = options[idx + 1:idx + num_generators + 1]
                with_generator = True
    if with_generator:
        x1, y1, z1, x2, y2, z2, x3, y3, z3 = 1, 1, 1, 1, 1, 1, 1, 1, 1
        if num_generators >= 1:
            generator_1 = generator_symbols[0].lower()
            if 'x' in generator_1:
                x1 = -1
            if 'y' in generator_1:
                y1 = -1
            if 'z' in generator_1:
                z1 = -1
        if num_generators >= 2:
            generator_2 = generator_symbols[1].lower()
            if 'x' in generator_2:
                x2 = -1
            if 'y' in generator_2:
                y2 = -1
            if 'z' in generator_2:
                z2 = -1
        if num_generators >= 3:
            generator_3 = generator_symbols[2].lower()
            if 'x' in generator_3:
                x3 = -1
            if 'y' in generator_3:
                y3 = -1
            if 'z' in generator_3:
                z3 = -1
    # element_number, x, y, z, label, atom_basis
    molecule_information: list[list[Any]] = []
    label2basis = {}
    for atomtype_idx in range(atomtypes):
        atom_options = next(lines).split()
        if old_format:
            atom_charge = int(float(atom_options[0]))
            number_atoms = int(atom_options[1])
            if with_atombasis:
                atom_basis = atom_options[2].split('=')[-1]
        else:
            for atom_option in atom_options:
                if 'atoms=' in atom_option.lower():
                    number_atoms = int(float(atom_option.split('=')[1]))
                elif 'basis=' in atom_option.lower():
                    atom_basis = atom_option.split('=')[1]
                elif 'charge=' in atom_option.lower():
                    atom_charge = int(float(atom_option.split('=')[1]))
        for atom_idx in range(number_atoms):
            atom_properties = next(lines).split()
            label = atom_properties[0]
            x, y, z = map(float, atom_properties[1:4])
            if not with_generator:
                molecule_information.append([atom_charge, x, y, z, label, atom_basis])
                if label not in label2basis:
                    label2basis[label] = [atom_basis]
                else:
                    if atom_basis not in label2basis[label]:
                        label2basis[label].append(atom_basis)
            else:
                new_coordinates = [[x, y, z]]
                next_coordinate = np.array([x1 * x, y1 * y, z1 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([x2 * x, y2 * y, z2 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([x3 * x, y3 * y, z3 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([x1 * x2 * x, y1 * y2 * y, z1 * z2 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([x2 * x3 * x, y2 * y3 * y, z2 * z3 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([x1 * x3 * x, y1 * y3 * y, z1 * z3 * z])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                next_coordinate = np.array([
                    x1 * x2 * x3 * x,
                    y1 * y2 * y3 * y,
                    z1 * z2 * z3 * z,
                ])
                if np.all(np.sum((new_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    new_coordinates.append(next_coordinate)
                for coordinate in new_coordinates:
                    x, y, z = coordinate
                    molecule_information.append([atom_charge, x, y, z, label, atom_basis])
                    if label not in label2basis:
                        label2basis[label] = [atom_basis]
                    else:
                        if atom_basis not in label2basis[label]:
                            label2basis[label].append(atom_basis)
    # Fix labels, https://gitlab.com/daltonproject/daltonproject/-/issues/5
    for idx in range(len(molecule_information)):
        label = molecule_information[idx][4]
        atom_basis = molecule_information[idx][5]
        if len(label2basis[label]) == 1:
            # If the len() is 1, then all with the label has the same basis.
            continue
        else:
            molecule_information[idx][4] = f'{label}{label2basis[label].index(atom_basis) + 1}'
    # Contruct the input for Molecule and basis
    atom_lines = ''
    for atom_charge, x, y, z, label, atom_basis in molecule_information:
        element = periodictable.to_symbol(atom_charge)
        x_mod = x * constants.conversion_factor(coordinate_unit, 'angstrom')
        y_mod = y * constants.conversion_factor(coordinate_unit, 'angstrom')
        z_mod = z * constants.conversion_factor(coordinate_unit, 'angstrom')
        atom_lines += f'{element} {x_mod} {y_mod} {z_mod} {label};'
        if with_atombasis:
            basis_set[label] = atom_basis
    # The -1 is to remove the trailing ";".
    molecule = Molecule(atoms=atom_lines[:-1], charge=molecule_charge)
    if with_atombasis:
        basis = Basis(basis=basis_set)
    else:
        basis = Basis(basis=basis_set['without_atombasis'])
    return molecule, basis
