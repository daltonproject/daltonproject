"""Molecule module."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

import os
import warnings
from collections.abc import Sequence

import numpy as np
import regex as re
from mendeleev.fetch import fetch_table
from qcelemental import periodictable

from .symmetry import SymmetryAnalysis


class Molecule:
    """Molecule class."""

    supported_filetypes = ('.xyz', '.mol', '.gau')

    def __init__(self,
                 atoms: str | None = None,
                 input_file: str | None = None,
                 charge: int = 0,
                 symmetry: bool = False,
                 multiplicity: int = 1,
                 isotopes: Sequence[str] | None = None) -> None:
        """Initialize Molecule instance.

        Args:
            atoms: Atoms specified with elements and coordinates (and optionally labels).
            charge: Total charge of molecule.
            input_file: Input file containing atoms and coordinates (and optionally labels).
            symmetry: Use symmetry detection.
            multiplicity: Multiplicity of molecule.
            isotopes: Seqeunce of isotopes.
        """
        if not isinstance(charge, int):
            raise TypeError('Charge must be an integer.')
        if isotopes is not None and not isinstance(isotopes, Sequence):
            raise TypeError('Isotopes must be a sequence of strings.')
        self.charge: int = charge
        self.elements: list[str] = []
        self.masses: np.ndarray = np.array([])
        self.labels: list[str] = []
        self.isotopes: list[str] = []
        self.isotope_order: list[int] = []
        self.mass_numbers: list[int] = []
        self.symmetry_threshold: float = 1e-3
        self.point_group: str = 'C1'
        self._coordinates: np.ndarray = np.array([])
        self.multiplicity: int = multiplicity
        if atoms is not None and input_file is not None:
            raise SyntaxError('Specify either atoms or input_file.')
        if atoms is None and input_file is None:
            raise SyntaxError('Specify either atoms or input_file.')
        if input_file is not None:
            if not os.path.isfile(input_file):
                raise FileNotFoundError(f'{input_file} not found.')
            file_name, file_ext = os.path.splitext(input_file)
            # check if file type is supported, i.e. is there a method corresponding to the file extension
            if hasattr(self, file_ext.strip('.')):
                # call method corresponding to file extension, e.g. if input_file has file extension '.xyz' then
                # this calls self.xyz(input_file)
                getattr(self, file_ext.strip('.'))(input_file)
            else:
                raise NotImplementedError(f'File type "{file_ext}" is not supported. Available file types are:\n'
                                          f'{self.supported_filetypes}')
        if atoms is not None:
            self.atoms(atoms)
        self.symmetry: SymmetryAnalysis = SymmetryAnalysis(self.elements,
                                                           self.coordinates,
                                                           symmetry_threshold=self.symmetry_threshold,
                                                           labels=self.labels)
        if symmetry:
            self.analyze_symmetry()
        if isotopes:
            if len(isotopes) != len(self.elements):
                raise TypeError(f'Number of isotopes {len(isotopes)} does not match number of '
                                f'atoms {len(self.elements)}.')
            for isotope in isotopes:
                if not re.match(r'^[a-zA-Z]+[0-9]+$', isotope):
                    raise TypeError(
                        f'Labels must be an atom followed by an isotope number e.g. O17.\nInstead found: {isotope}.')
        else:
            isotope_masses = [periodictable.to_mass(element) for element in self.elements]
            isotopes = [f'{label}{round(mass)}' for label, mass in zip(self.elements, isotope_masses)]
        self.isotopes_assign(isotopes)

    def __repr__(self) -> str:
        """Return a printable representation of the Molecule instance."""
        repr_str = (f'Molecule(num_atoms={self.num_atoms},'
                    f' charge={self.charge},'
                    f' point_group={self.point_group},'
                    f' elements={self.elements},'
                    f' coordinates={self.coordinates.tolist()},'
                    f' labels={self.labels},'
                    f' masses={self.masses.tolist()}),'
                    f' multiplicity={self.multiplicity}')
        return repr_str

    def __add__(self, other: Molecule) -> Molecule:
        """Add two Molecule instances."""
        if not isinstance(other, Molecule):
            raise TypeError(f'Cannot add "{type(self).__name__}" and "{type(other).__name__}" types.')
        new_charge = self.charge + other.charge
        new_atoms = ''
        for element, coordinate, label in zip(self.elements, self.coordinates, self.labels):
            new_atoms += f' {element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label};'
        for element, coordinate, label in zip(other.elements, other.coordinates, other.labels):
            new_atoms += f' {element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label}; '
        new_molecule = Molecule(atoms=new_atoms, charge=new_charge)
        return new_molecule

    def atoms(self, atoms: str) -> None:
        """Specify atoms.

        Args:
            atoms: Atoms specified with elements and coordinates (and optionally labels).
        """
        if not isinstance(atoms, str):
            raise TypeError('Atoms are given as a string using either "\\n" or ";" to separate atoms.')
        # get rid of final new line if present
        if atoms[-1:] == '\n':
            atoms = atoms[:-1]
        if ';' in atoms:
            lines = atoms.split(';')
        elif '\n' in atoms:
            lines = atoms.split('\n')
        elif not 4 <= len(atoms.split()) <= 5:
            raise ValueError(f'Incompatible input: {atoms}')
        else:
            # Assume it is a single atom.
            lines = [atoms]
        if lines[-1:] == ['']:
            lines = lines[:-1]
        elements = []
        masses = []
        labels = []
        coordinates = []
        for line in lines:
            atom = line.split()
            if not atom:
                continue
            element = atom[0].strip().title()
            if element not in periodictable.E:
                raise ValueError(f'Unknown element: {element}.')
            elements.append(element)
            masses.append(periodictable.to_mass(element))
            coordinates.append(np.array([float(value) for value in atom[1:4]]))
            if len(atom) == 5:
                labels.append(atom[4].strip())
            else:
                labels.append(atom[0].strip())
        self.elements = elements
        self.masses = np.array(masses)
        self.labels = labels
        self.coordinates = np.array(coordinates)

    def xyz(self, filename: str) -> None:
        """Read xyz file.

        Args:
            filename: Name of xyz file containing atoms and coordinates.
        """
        with open(f'{filename}', 'r') as xyz_file:
            lines = xyz_file.read().split('\n')
        try:
            num_atoms = int(lines[0])
        except ValueError:
            raise TypeError('First line in xyz file cannot be converted to an integer.')
        # Remove trailing empty lines.
        while lines[-1].strip() == '':
            del lines[-1]
        if len(lines[2:]) > num_atoms:
            warnings.warn(f'Only {num_atoms} atoms were declared but xyz file apparently contains {len(lines[2:])}'
                          f' lines. Only {num_atoms} will be read.')
        if len(lines[2:]) < num_atoms:
            raise ValueError(f'{num_atoms} atoms were declared, but file only contains {len(lines[2:])} lines.')
        atom_lines = ''
        # Skip the declaration and comment line.
        for line in lines[2:num_atoms + 2]:
            coordinate_line = line.split()
            # We allow lines to be longer than 4 and 5 values in case the user adds comments etc.
            if len(coordinate_line) < 4:
                raise ValueError(f'Wrong number of values in line:\n{line}')
            if coordinate_line[0].strip().title() not in periodictable.E:
                raise ValueError(f'Unknown element in line:\n{line}.')
            try:
                [float(value) for value in coordinate_line[1:4]]
            except ValueError:
                raise TypeError(f'Cannot read coordinates from line:\n{line}')
            # Add five values in case there are atom labels but remove the rest (if present).
            atom_lines += ' '.join(coordinate_line[0:5]) + '\n'
        # The slice is to remove the last new-line character.
        self.atoms(atom_lines[:-1])

    def mol(self, filename: str) -> None:
        """Read dalton mol file.

        Args:
            filename: Name of mol file containing charge, atoms, and coordinates.
        """
        from .mol_reader import mol_reader
        molecule, basis = mol_reader(filename)
        self.elements = molecule.elements
        self.masses = molecule.masses
        self.labels = molecule.labels
        self.coordinates = molecule.coordinates
        self.charge = molecule.charge

    def gau(self, filename: str) -> None:
        """Read Gaussian input file.

        Args:
            filename: Name of Gaussian input file containing charge, atoms, and coordinates.

        """
        with open(f'{filename}', 'r') as gaus_file:
            lines = gaus_file.read().split('\n')
        try:
            num_atoms = int(lines[0])
        except ValueError:
            raise TypeError('First line in gaus file cannot be converted to an integer.')
        while lines[-1].strip() == '':
            del lines[-1]
        if len(lines[3:]) > num_atoms:
            warnings.warn(f'Only {num_atoms} atoms were declared but gaus file apparently contains {len(lines[3:])}'
                          f' lines. Only {num_atoms} will be read.')
        if len(lines[3:]) < num_atoms:
            raise ValueError(f'{num_atoms} atoms were declared, but file only contains {len(lines[3:])} lines.')
        atom_lines = ''
        if lines[2] == '':
            raise ValueError('Charge and multiplicity has not been given correctly. Charge and multiplicity must be \
                             given on the third row, directly above the atomic coordinates')
        charge = int(lines[2].split(' ')[0])
        multiplicity = int(lines[2].split(' ')[1])
        for line in lines[3:num_atoms + 3]:
            coordinate_line = line.split()
            if len(coordinate_line) < 4:
                raise ValueError(f'Wrong number of values in line:\n{line}')
            if coordinate_line[0].strip().title() not in periodictable.E:
                raise ValueError(f'Unknown element in line:\n{line}.')
            try:
                [float(value) for value in coordinate_line[1:4]]
            except ValueError:
                raise TypeError(f'Cannot read coordinates from line:\n{line}')
            atom_lines += ' '.join(coordinate_line[0:5]) + '\n'
        self.atoms(atom_lines[:-1])
        self.charge = charge
        self.multiplicity = multiplicity

    @property
    def coordinates(self) -> np.ndarray:
        """Atomic coordinates."""
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coordinates: Sequence[Sequence[float]] | np.ndarray) -> None:
        if not len(coordinates) == self.num_atoms:
            raise Exception('Number of coordinates does not correspond to number of atoms in molecule.')
        if isinstance(coordinates, np.ndarray):
            self._coordinates = coordinates
        elif isinstance(coordinates, Sequence):
            self._coordinates = np.array(coordinates)

    @property
    def num_atoms(self) -> int:
        """Return the number of atoms in the molecule."""
        return len(self.elements)

    def analyze_symmetry(self) -> None:
        """Analyze the molecular symmetry. Note that this translates the molecule's center of mass to the origin."""
        self.symmetry = SymmetryAnalysis(self.elements,
                                         self.coordinates,
                                         symmetry_threshold=self.symmetry_threshold,
                                         labels=self.labels)
        self.coordinates, self.point_group = self.symmetry.find_symmetry()

    def isotopes_assign(self, isotopes: Sequence[str]) -> None:
        """Assign isotope order according to abundance.

        Args:
            isotopes: Isotopes of the nuclei.
        """
        isotope_order = []
        mass_numbers = []
        isotopes_tab = fetch_table('isotopes')
        for isotope in isotopes:
            atom = ''.join(filter(lambda x: x.isalpha(), isotope))
            isotope_num = isotope.strip(atom)
            atomic_number = periodictable.to_atomic_number(atom)
            all_abundances = isotopes_tab[isotopes_tab['atomic_number'] == atomic_number]['abundance'].tolist()
            abundances = [x for x in all_abundances if not np.isnan(x)]
            df = isotopes_tab[(isotopes_tab['atomic_number'] == atomic_number)
                              & (isotopes_tab['mass_number'] == int(isotope_num))]
            if not re.search(r'\d', df['abundance'].to_string(index=False)):
                raise TypeError(f'{isotope_num} not an isotope of {atom}.')
            ordered_isotope_properties = sorted(abundances, reverse=True)
            mass_numbers.append(int(df['mass_number'].to_string(index=False)))
            isotope_order.append(
                ordered_isotope_properties.index(float(df['abundance'].to_string(index=False))) + 1)
        self.mass_numbers = mass_numbers
        self.isotope_order = isotope_order
        self.isotopes = list(isotopes)
