"""Vibrational analysis."""
#  Dalton Project: A Python platform for molecular- and electronic-structure
#  simulations of complex systems
#
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

from collections.abc import Sequence
from typing import NamedTuple

import numpy as np
from qcelemental import PhysicalConstantsContext, periodictable
from spectroscpy import get_raman_intensities, get_vib_harm_freqs_and_eigvecs

from .molecule import Molecule
from .program import PolarizabilityGradients

__all__ = ('VibrationalAnalysis', 'vibrational_analysis', 'compute_ir_intensities', 'compute_raman_intensities',
           'normal_coords', 'normal_mode_eigensolver', 'number_modes')

constants = PhysicalConstantsContext('CODATA2018')


class VibrationalAnalysis(NamedTuple):
    """Data structure for vibrational properties."""

    frequencies: np.ndarray
    ir_intensities: np.ndarray | None = None
    raman_intensities: np.ndarray | None = None
    cartesian_displacements: np.ndarray | None = None


class NormalModeEigenSolution(NamedTuple):
    """Data structure for Hessian Eigen results."""

    eigenvalues: np.ndarray
    transformation_matrix: np.ndarray
    frequencies: np.ndarray


def vibrational_analysis(molecule: Molecule,
                         hessian: np.ndarray,
                         dipole_gradients: np.ndarray | None = None,
                         polarizability_gradients: PolarizabilityGradients | None = None) -> VibrationalAnalysis:
    """Perform vibrational analysis.

    Args:
        molecule: Molecule object.
        hessian: Second derivatives with respect to nuclear displacements in
            Cartesian coordinates.
        dipole_gradients: Gradient of the dipole moment with respect to nuclear
            displacements in Cartesian coordinates.
        polarizability_gradients: Gradient of the polarizability with respect to
            nuclear displacements in Cartesian coordinates.

    Returns:
        Harmonic vibrational frequencies (cm^-1). Optionally, IR intensities
        (m^2 / (s * mol)), Raman intensities (C^4 * s^2 / (J * m^2 * kg)), and
        transformation matrix.
    """
    coordinates = molecule.coordinates / constants.bohr2angstroms
    atomic_numbers = np.array([periodictable.to_atomic_number(element) for element in molecule.elements])
    energies, transformation_matrix, _ = get_vib_harm_freqs_and_eigvecs(coords=coordinates,
                                                                        charges=atomic_numbers,
                                                                        masses=molecule.masses,
                                                                        hess=hessian,
                                                                        outproj=True,
                                                                        print_level=0)
    vibrational_frequencies = energies * constants.hartree2wavenumbers
    ir_intensities = None
    if dipole_gradients is not None:
        ir_intensities = compute_ir_intensities(dipole_gradients, transformation_matrix)
    raman_intensities = None
    if polarizability_gradients is not None:
        raman_intensities = []
        for gradients, frequency in zip(polarizability_gradients.values, polarizability_gradients.frequencies):
            raman_intensities.append(
                compute_raman_intensities(gradients, frequency, vibrational_frequencies, transformation_matrix))
        raman_intensities = np.array(raman_intensities)
    return VibrationalAnalysis(frequencies=vibrational_frequencies,
                               ir_intensities=ir_intensities,
                               raman_intensities=raman_intensities,
                               cartesian_displacements=transformation_matrix)


def compute_ir_intensities(dipole_gradients: np.ndarray,
                           transformation_matrix: np.ndarray,
                           is_linear: bool = False) -> np.ndarray:
    """Compute infrared molar decadic absorption coefficient in m^2 / (s * mol).

    Args:
        dipole_gradients: Gradients of the dipole moment (au) with respect to
            nuclear displacements in Cartesian coordinates.
        transformation_matrix: Transformation matrix to convert from Cartesian
            to normal coordinates.
        is_linear: Indicate if the molecule is linear.

    Returns:
        IR intensities in m^2 / (s * mol).
    """
    normal_dipole_gradients = transformation_matrix.T @ dipole_gradients
    if is_linear:
        normal_dipole_gradients = normal_dipole_gradients[:-5]
    else:
        normal_dipole_gradients = normal_dipole_gradients[:-6]
    ir_intensities = []
    normal_dipole_gradients_si = normal_dipole_gradients * constants.get('elementary charge') / np.sqrt(constants.me)
    prefactor = constants.na / (12.0 * np.log(10) * constants.e0 * constants.c)
    for dipole_gradient in normal_dipole_gradients_si:
        ir_intensities.append(prefactor * np.sum(dipole_gradient**2))
    return np.array(ir_intensities)


def compute_raman_intensities(polarizability_gradients: np.ndarray,
                              polarizability_frequency: float,
                              vibrational_frequencies: np.ndarray,
                              transformation_matrix: np.ndarray,
                              is_linear: bool = False) -> np.ndarray:
    """Compute Raman differential scattering cross-section in C^4 * s^2 / (J * m^2 * kg).

    Args:
        polarizability_gradients: Gradients of the polarizability with respect
            to nuclear displacements in Cartesian coordinates.
        polarizability_frequency: Frequency of the incident light in au.
        vibrational_frequencies: Vibrational frequencies in cm^-1.
        transformation_matrix: Transformation matrix to convert from Cartesian
            to normal coordinates.
        is_linear: Indicate if the molecule is linear.

    Returns:
        Raman intensities in C^4 * s^2 / (J * m^2 * kg).
    """
    normal_polar_gradient = np.zeros(polarizability_gradients.shape)
    for i in range(3):
        normal_polar_gradient[:, :, i] = transformation_matrix.T @ polarizability_gradients[:, :, i]
    if is_linear:
        normal_polar_gradient = normal_polar_gradient[:-5, :, :]
    else:
        normal_polar_gradient = normal_polar_gradient[:-6, :, :]
    vibrational_energies = vibrational_frequencies / constants.hartree2wavenumbers
    raman_intensities = get_raman_intensities(specifications=['Raman: SCS 45+7, SI arb units'],
                                              au_incident_vibration_energy=polarizability_frequency,
                                              au_polarizability_gradients=normal_polar_gradient,
                                              au_vibrational_energies=vibrational_energies,
                                              print_level=0,
                                              temperature=298.15)
    return np.array(raman_intensities)


def normal_coords(molecule: Molecule,
                  hessian: np.ndarray,
                  is_linear: bool = False,
                  isotopes: Sequence[float] | None = None) -> np.ndarray:
    """Compute normal coordinates.

    Args:
        molecule: Molecule object.
        hessian: Second derivatives of energy with respect to nuclear displacements.
        is_linear: Indicate if the molecule is linear.
        isotopes: List of isotopes for each atom in the molecule.

    Returns:
        q_mat = normal mode coordinates.
    """
    _, eign_vects, _ = normal_mode_eigensolver(molecule, hessian, is_linear, isotopes)
    m_mat = diagonal_mass_matrix(molecule, isotopes)
    q_mat = eign_vects.T @ m_mat
    return q_mat


def normal_mode_eigensolver(molecule: Molecule,
                            hessian: np.ndarray,
                            is_linear: bool = False,
                            isotopes: Sequence[float] | None = None) -> NormalModeEigenSolution:
    """Calculate eigenvalues and eigenvectors of the Hessian matrix.

    Args:
        molecule: Molecule object.
        hessian: Second derivatives of energy with respect to nuclear displacements in Cartesian coordinates.
        is_linear: Indicate if the molecule is linear.
        isotopes: List of isotopes for each atom in the molecule.

    Returns:
        eigen_vals: Eigenvalues of the mass-weighted Hessian matrix.
        transformation_matrix: Eigenvectors of the mass-weighted Hessian matrix.
        frequencies: Vibrational frequencies in cm^-1.
    """
    num_modes = number_modes(molecule, is_linear)
    m_mat = diagonal_mass_matrix(molecule, isotopes)
    mass_weighted_hessian = m_mat @ hessian @ m_mat
    eigen_vals, transformation_matrix = np.linalg.eigh(mass_weighted_hessian)
    sort_list = list(eigen_vals)
    sort_list.sort()
    for e in range(len(eigen_vals) - 1, -1, -1):
        if np.abs(eigen_vals[e]) not in sort_list[-num_modes:]:
            eigen_vals = np.delete(eigen_vals, e)
            transformation_matrix = np.delete(transformation_matrix, e, 1)
    frequencies = (eigen_vals * constants.na * constants.hartree2J * 1000)**0.5
    frequencies *= (1 / constants.bohr2cm**2)**0.5 / (2 * np.pi * constants.c)
    return NormalModeEigenSolution(eigen_vals, transformation_matrix, frequencies)


def number_modes(molecule: Molecule, is_linear: bool) -> int:
    """Return number of normal modes.

    Args:
        molecule: Molecule object.
        is_linear: Indicate if the molecule is linear.

    Returns:
        num_modes: Number of normal modes.
    """
    if is_linear:
        num_modes = 3 * len(molecule.labels) - 5
    else:
        num_modes = 3 * len(molecule.labels) - 6
    return num_modes


def diagonal_mass_matrix(molecule: Molecule, isotopes: Sequence[float] | None = None) -> np.ndarray:
    """Return diagonal mass matrix given as 1/sqrt(mass)

    Args:
        molecule: Molecule object.
        isotopes: Sequence of isotopes for each atom in the molecule.

    Returns:
        m_mat: Diagonal mass matrix.
    """
    if isotopes is not None:
        atomic_masses = isotopes
    else:
        atomic_masses = [periodictable.to_mass(element) for element in molecule.elements]
    m_vec = np.repeat(np.array([(1 / atomic_mass)**0.5 for atomic_mass in atomic_masses]), 3)
    m_mat = np.diagflat(m_vec)
    return m_mat
