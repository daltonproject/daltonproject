"""Parse the Gaussian output files."""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

import warnings

import numpy as np
import regex as re
from qcelemental import PhysicalConstantsContext

from .. import program
from ..program import OpticalRotations, Polarizabilities

constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(program.OutputParser):
    """Parse the gaussian output files."""

    def __init__(self, filename: str) -> None:
        """Initialize gaussian output parser."""
        self._filename = filename

    @property
    def filename(self) -> str:
        return self._filename

    @property
    def energy(self) -> float:
        """Extract energy from the formatted gaussian output file."""
        with open(f'{self.filename}.fchk', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'SCF Energy' in line:
                energy = float(line.split()[3])
                break
        return energy

    @property
    def hessian(self) -> np.ndarray:
        """Extract Hessian matrix from the gaussian formatted check point file."""
        with open(f'{self.filename}.fchk') as file:
            lines = file.readlines()
        for line in lines:
            if 'Number of atoms' in line:
                num_atoms = int(line.split()[4])
                break
        cartesian_str = []
        for num, line in enumerate(lines):
            if 'Cartesian Force Constants' in line:
                elements = line.split()[5]
                rows = int(elements) // 5
                if int(elements) % 5 != 0:
                    rows += 1
                for row in range(int(rows)):
                    cartesian_str += lines[num + 1 + row].split()
        lower_triangle = np.zeros((3 * num_atoms, 3 * num_atoms))
        j = 0
        k = 0
        for i in range(3 * num_atoms):
            k += i + 1
            lower_triangle[i, 0:k - j] = np.array([cartesian_str[j:k]]).astype(float)
            j = k
        hessian = lower_triangle + lower_triangle.T - np.diag(lower_triangle.diagonal())
        return hessian

    @property
    def nmr_shieldings(self) -> np.ndarray:
        """Extract NMR shielding constants from the gaussian formatted checkpoint file."""
        with open(f'{self.filename}.fchk', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'Number of atoms' in line:
                num_atoms = int(line.split()[4])
                break
        nmr_shieldings_array = []
        nmr_shieldings = np.zeros(num_atoms)
        for idx, line in enumerate(lines):
            if 'NMR shielding' in line:
                count_rows = (int(line.split()[4]) // 5)
                if int(line.split()[4]) % 5 != 0:
                    count_rows += 1
                for row in range(int(count_rows)):
                    nmr_shieldings_array += lines[idx + 1 + row].split()
        nmr_shieldings_array = np.array(nmr_shieldings_array).astype(float).reshape(num_atoms, 3, 3)
        for i in range(num_atoms):
            nmr_shieldings[i] = np.trace(nmr_shieldings_array[i]) / 3
        return nmr_shieldings * 53.25136  # convert to ppm

    @property
    def final_geometry(self) -> np.ndarray:
        """Extract final geometry from the gaussian output file."""
        with open(f'{self.filename}.out', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'NAtoms' in line:
                num_atoms = int(line.split()[1])
                break
        converged = False
        for idx, line in enumerate(lines):
            if 'Converged?' in line:
                if 'YES' in lines[idx + 1] and 'YES' in lines[idx + 2] and 'YES' in lines[
                        idx + 3] and 'YES' in lines[idx + 4]:
                    converged = True
                    break
        if not converged:
            raise RuntimeError('Geometry optimization failed to converge to a minimum.')
        for num, line in enumerate(lines):
            if 'Input orientation:' in line:
                start = num
        coordinates = []
        for line in lines[start + 5:start + num_atoms + 5]:
            coordinates.append([float(i) for i in line.split()[3:]])
        return np.array(coordinates)

    @property
    def polarizabilities(self) -> Polarizabilities:
        """Extract polarizability tensor(s) from the gaussian formatted checkpoint file."""

        def tensor_func(line: list[str]) -> np.ndarray:
            tensor = np.zeros((3, 3))
            values = list(map(float, line))
            tensor[0, 0] = values[0]
            tensor[0, 1] = tensor[1, 0] = values[1]
            tensor[0, 2] = tensor[2, 0] = values[2]
            tensor[1, 1] = values[4]
            tensor[1, 2] = tensor[2, 1] = values[5]
            tensor[2, 2] = values[8]
            return tensor

        with open(f'{self.filename}.fchk', 'r') as output_file:
            lines = output_file.readlines()
        dyanmic_polarizability = False
        for idx, line in enumerate(lines):
            line_str = []
            if 'Frequencies for FD properties' in line:
                num_lines = int(line.split()[6]) // 5
                if int(line.split()[6]) % 5 != 0:
                    num_lines += 1
                for row in range(int(num_lines)):
                    line_str += lines[idx + 1 + row].split()
                frequencies = np.array(line_str).astype(float)
            elif 'Alpha(-w,w)' in line:
                num_lines = int(line.split()[3]) // 5
                if int(line.split()[3]) % 5 != 0:
                    num_lines += 1
                for row in range(int(num_lines)):
                    line_str += lines[idx + 1 + row].split()
                num_tensors = int(line.split()[3]) // 9
                polarizability = np.zeros((num_tensors, 3, 3))
                for i in range(int(num_tensors)):
                    polarizability[i] = tensor_func(line_str[9 * i:9 * (i+1)])
                dyanmic_polarizability = True
                break
        if not dyanmic_polarizability:
            polarizability = np.zeros((1, 3, 3))
            for idx, line in enumerate(lines):
                if 'Polarizability' in line:
                    polarizability[0, 0, 0] = float(lines[idx + 1].split()[0])
                    polarizability[0, 0, 1] = polarizability[0, 1, 0] = float(lines[idx + 1].split()[1])
                    polarizability[0, 1, 1] = float(lines[idx + 1].split()[2])
                    polarizability[0, 0, 2] = polarizability[0, 2, 0] = float(lines[idx + 1].split()[3])
                    polarizability[0, 1, 2] = polarizability[0, 2, 1] = float(lines[idx + 1].split()[4])
                    polarizability[0, 2, 2] = float(lines[idx + 2].split()[0])
                    break
            frequencies = np.array([0.0])
        return Polarizabilities(frequencies=frequencies, values=polarizability)

    @property
    def hyperfine_couplings(self) -> np.ndarray:
        """Extract hyperfine couplings in atomic units from gaussian formatted checkpoint file."""
        with open(f'{self.filename}.fchk', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'Number of atoms' in line:
                num_atoms = float(line.split()[4])
                break
        couplings = []
        count_lines = num_atoms % 6
        for idx, line in enumerate(lines):
            if 'Isotropic Hyperfine splittings' in line:
                for row in range(int(count_lines) + 1):
                    couplings += lines[idx + 1 + row].split()
        return np.array(couplings).astype(float)

    @property
    def spin_spin_couplings(self) -> np.ndarray:
        """Extract contributions to spin-spin coupling constants from gaussian output file."""
        sscc = []
        dia_spin_orbit: list[str] = []
        para_spin_orbit: list[str] = []
        spin_dipole: list[str] = []
        fermi_contact: list[str] = []
        search_terms = [
            ('Diamagnetic spin-orbit (DSO) contribution to J (Hz):', 'Total nuclear spin-spin coupling K (Hz):',
             dia_spin_orbit),
            ('Paramagnetic spin-orbit (PSO) contribution to J (Hz):',
             'Diamagnetic spin-orbit (DSO) contribution to K (Hz):', para_spin_orbit),
            ('Spin-dipolar (SD) contribution to J (Hz):', 'Paramagnetic spin-orbit (PSO) contribution to K (Hz):',
             spin_dipole),
            ('Fermi Contact (FC) contribution to J (Hz):', 'Spin-dipolar (SD) contribution to K (Hz):',
             fermi_contact),
        ]
        with open(f'{self.filename}.out', 'r') as output_file:
            lines = output_file.readlines()
        for idx, line in enumerate(lines):
            for start, stop, lst in search_terms:
                if start in line:
                    for line in lines[idx + 2:]:
                        if stop in line:
                            break
                        # skip line if just indices
                        if 'D' not in line.split()[1]:
                            continue
                        lst += line.split()[1:]
        for start, _, lst in search_terms:
            if len(lst) == 0:
                warnings.warn(f'Could not find {start}.')
            else:
                while (lst.count('0.000000D+00')):
                    lst.remove('0.000000D+00')
                sscc.append([num.replace('D', 'E') for num in lst])
        return np.array(sscc).astype(float).reshape(len(sscc), len(sscc[0]))

    @property
    def spin_spin_labels(self) -> list[str]:
        """Extract spin-spin coupling labels from gaussian output file."""
        property_labels: list[str] = []
        atoms = []
        with open(f'{self.filename}.out', 'r') as output_file:
            lines = output_file.readlines()
        for idx, line in enumerate(lines):
            if 'Distance matrix (angstroms):' in line:
                for line in lines[idx + 2:]:
                    if not re.search(r'\d', line.split()[2]):
                        break
                    atoms.append(line.split()[1])
        for i, atom1 in enumerate(atoms):
            for atom2 in atoms[:i]:
                property_labels.append(f'{atom1} {atom2}')
        return property_labels

    @property
    def optical_rotations(self) -> OpticalRotations:
        """Extract optical rotation from gaussian output file."""

        def get_tensor(line: list[str]) -> np.ndarray:
            values = list(map(float, line))
            tensor = np.array([[values[0], values[3], values[6]], [values[1], values[4], values[7]],
                               [values[2], values[5], values[8]]])
            return tensor

        with open(f'{self.filename}.fchk', 'r') as output_file:
            lines = output_file.readlines()
        frequencies = []
        optical_rotations = []
        for idx, line in enumerate(lines):
            line_str = []
            if 'Frequencies for FD properties' in line:
                num_lines = int(line.split()[6]) // 5
                if int(line.split()[6]) % 5 != 0:
                    num_lines += 1
                for row in range(int(num_lines)):
                    line_str += lines[idx + 1 + row].split()
                frequencies = np.array(line_str).astype(float)
            elif 'FD Optical Rotation Tensor' in line:
                num_lines = int(line.split()[6]) // 5
                if int(line.split()[6]) % 5 != 0:
                    num_lines += 1
                for row in range(int(num_lines)):
                    line_str += lines[idx + 1 + row].split()
                num_tensors = int(line.split()[6]) // 9
                optical_rotations = np.zeros((num_tensors, 3, 3))
                for i in range(int(num_tensors)):
                    optical_rotations[i] = get_tensor(line_str[9 * i:9 * (i+1)])
        return OpticalRotations(frequencies=frequencies, values=optical_rotations)
