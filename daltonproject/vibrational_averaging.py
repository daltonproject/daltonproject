"""Main driver for calculation of vibrational corrections to properties."""
#  Dalton Project: A Python platform for molecular- and electronic-structure
#  simulations of complex systems
#
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

import os
from typing import NamedTuple, Sequence

import numpy as np
import numpy.polynomial.polynomial as poly
import yaml
from qcelemental import PhysicalConstantsContext, periodictable

from .molecule import Molecule
from .numerical_derivatives import linear_cfcs, linear_property, polynomial_cfcs, polynomial_property
from .program import OutputParser
from .property import Property
from .symmetry import SymmetryAnalysis
from .vibrational_analysis import diagonal_mass_matrix, normal_coords, normal_mode_eigensolver

constants = PhysicalConstantsContext('CODATA2018')


class VibAvConstants:
    """Modified constants used in vibrational averaging."""

    @property
    def hbar_a(self) -> float:
        """Return reduced Planck constant in units of angstrom^2 * kg/s."""
        return constants.hbar * 1e20

    @property
    def c_cm(self) -> float:
        """Return speed of light in units of cm/s."""
        return constants.c * 1e2

    @property
    def m2au(self) -> float:
        """Convert from meter to bohr."""
        return 1e2 / constants.bohr2cm


vibav_constants = VibAvConstants()


class VibAvSettings:
    """VibAvSettings class for initialising user input and generating distorted
        geometry .xyz or .gau files for property and Hessian calculations."""

    def __init__(self,
                 molecule: Molecule,
                 property_program: str,
                 is_mol_linear: bool,
                 hessian: np.ndarray,
                 property_obj: Property,
                 stepsize: float | int,
                 differentiation_method: str,
                 temperature: float | int,
                 polynomial_fitting_order: int | None = None,
                 linear_point_stencil: int | None = None,
                 plot_polyfittings: bool | None = False) -> None:
        """Initialize VibAvSettings instance.

        Args:
            molecule: Molecule class object.
            property_program: Program used to calculate property.
            is_mol_linear: Boolean indicating if molecule is linear.
            hessian: Equilibrium Hessian matrix.
            property_obj: Property object.
            stepsize: Step size (in reduced normal coordinates).
            differentiation_method: Method for finite difference (polynomial fitting/linear point stencil).
            temperature: Temperature in Kelvin.
            polynomial_fitting_order: Order of fitting for differentiation method.
            linear_point_stencil: Number of points for linear fitting.
            plot_polyfittings: Plot polynomial fitting curves.
        """
        self.fitting_order: int = 0
        supported_programs = ('dalton', 'gaussian')
        self.property_program = property_program.lower()
        if self.property_program not in supported_programs:
            raise TypeError(f'Property program must be one of {supported_programs}, got {self.property_program}')
        self.property_obj = property_obj
        self.check_property_assignment()
        if not isinstance(molecule, Molecule):
            raise TypeError('Molecule must be a Molecule class object.')
        self.molecule = molecule
        if not isinstance(is_mol_linear, bool):
            raise TypeError(f'is_mol_linear must be a boolean, got {is_mol_linear}')
        self.is_mol_linear = is_mol_linear
        if not isinstance(hessian, np.ndarray):
            raise TypeError(f'Type of Hessian must be a numpy array, got {type(hessian)}')
        self.hessian = hessian
        if not isinstance(property_obj, Property):
            raise TypeError('Property must be a Property class object.')
        if not isinstance(stepsize, (float, int)):
            raise TypeError(f'Stepsize must be a float/integer, got {type(stepsize)}')
        self.stepsize = stepsize
        if differentiation_method not in ['linear', 'polynomial']:
            raise TypeError(f'differentiation_method must be "linear" or "polynomial", got {differentiation_method}')
        if linear_point_stencil and polynomial_fitting_order:
            raise TypeError('Allowed to only specify linear_point_stencil or polynomial_fitting_order.')
        if differentiation_method == 'linear' and linear_point_stencil is None:
            raise TypeError('Must specify linear stencil number.')
        if differentiation_method == 'polynomial' and polynomial_fitting_order is None:
            raise TypeError('Must specify polynomial fitting order.')
        self.differentiation_method = differentiation_method
        if polynomial_fitting_order:
            if not isinstance(polynomial_fitting_order, int):
                raise TypeError(f'polynomial_fitting_order must be an integer, got {type(polynomial_fitting_order)}')
        self.polynomial_fitting_order = polynomial_fitting_order
        if linear_point_stencil:
            if not isinstance(linear_point_stencil, int):
                raise TypeError(f'linear_point_stencil must be an integer, got {type(linear_point_stencil)}')
        if differentiation_method == 'linear':
            if linear_point_stencil not in [3, 5]:
                raise TypeError(f'Linear fitting order must be 3 or 5, got {linear_point_stencil}')
        self.linear_point_stencil = linear_point_stencil
        if not isinstance(temperature, (float, int)):
            raise TypeError(f'temperature must be a float or integer, got {type(temperature)}')
        self.temperature = temperature
        if not isinstance(plot_polyfittings, bool):
            raise TypeError(f'plot_polyfittings must be a boolean, got {type(plot_polyfittings)}')
        self.plot_polyfittings = plot_polyfittings
        if linear_point_stencil:
            self.fitting_order = linear_point_stencil
        elif polynomial_fitting_order:
            self.fitting_order = polynomial_fitting_order
        self.isotopes = [periodictable.to_mass(element) for element in molecule.isotopes]
        self.file_list = self.generate_displaced_geometries()

    def check_property_assignment(self):
        """Check to see if property and program combination is supported.
        """
        self.property_str: list[str] | str = ''
        if 'nmr_shieldings' in self.property_obj.settings:
            if self.property_program not in ('dalton', 'gaussian'):
                raise TypeError(f'Vibrational averaging of shieldings \
                                with {self.property_program} not supported.')
            self.property_str = 'Iso. Shieldings'
        elif 'polarizabilities' in self.property_obj.settings:
            if self.property_program not in ('dalton', 'gaussian'):
                raise TypeError(f'Vibrational averaging of polarizabilities \
                                with {self.property_program} not supported.')
            self.property_str = 'Polarizability'
        elif 'spin_spin_couplings' in self.property_obj.settings:
            if self.property_program not in ('dalton', 'gaussian'):
                raise TypeError(
                    f'Vibrational averaging of spin spin couplings with {self.property_program} not supported.')
            self.property_str = ['DSO Corr.', 'PSO Corr.', 'SD Corr.', 'FC Corr.']
        elif 'hyperfine_couplings' in self.property_obj.settings:
            if self.property_program not in ('gaussian'):
                raise TypeError(
                    f'Vibrational averaging of hyperfine couplings with {self.property_program} not supported.')
            self.property_str = 'Hyper. Couplings'
        elif 'optical_rotations' in self.property_obj.settings:
            if self.property_program not in ('dalton', 'gaussian'):
                raise TypeError(f'Vibrational averaging of optical rotations with \
                                {self.property_program} not supported.')
            self.property_str = 'Opt. Rotation'

    def generate_displaced_geometries(self) -> list[str]:
        """Write loop for displaced geometries.

        Returns:
            file_list: Sequence of filenames for displaced geometries.
        """
        if self.fitting_order == 3 and self.differentiation_method == 'linear':
            displace_tag = [('minus', -1), ('plus', 1)]
        else:
            displace_tag = [('minus', -2), ('minus', -1), ('plus', 1), ('plus', 2)]
        self.q_mat = normal_coords(self.molecule, self.hessian, self.is_mol_linear, self.isotopes)
        self.eigen_solutions = normal_mode_eigensolver(self.molecule, self.hessian, self.is_mol_linear,
                                                       self.isotopes)
        self.eq_freqs = self.eigen_solutions.frequencies
        if any(i < 0.0 for i in self.eq_freqs):
            raise ValueError('Imaginary frequency detected. Cannot proceed with vibrational averaging.')
        num_modes = len(self.eq_freqs)
        reordered_q_mat = np.zeros((3 * num_modes, self.molecule.num_atoms))
        # reorder q_mat so that each row corresponds to a mode and axis of coordinates i.e  mode 1, x coords
        for i in range(num_modes):
            for j in range(3):
                reordered_q_mat[(i*3) + j] = self.q_mat[i, j:self.molecule.num_atoms * 3:3]
        file_list = []
        for nmode in range(num_modes):
            for tag, factor in displace_tag:
                coords = self.displace_coordinates(factor, self.eq_freqs[nmode], nmode, reordered_q_mat)
                if self.property_program == 'gaussian':
                    file = f'mode_{nmode}_{tag}{abs(factor)}.gau'
                    self.write_gau_input(coords, file)
                else:
                    file = f'mode_{nmode}_{tag}{abs(factor)}.xyz'
                    self.write_xyz_input(coords, file)
                file_list.append(file)
        if self.property_program == 'gaussian':
            self.write_gau_input(self.molecule.coordinates, 'standard_property.gau')
            file_list.append('standard_property.gau')
        else:
            self.write_xyz_input(self.molecule.coordinates, 'standard_property.xyz')
            file_list.append('standard_property.xyz')
        return file_list

    def displace_coordinates(self, factor: int, eq_freq: float, mode_num: int,
                             reordered_q_mat: np.ndarray) -> np.ndarray:
        """Displace equilibrium coordinates.

        Args:
            factor: Factor to multiply normal coordinate by.
            eq_freq: An equilibrium frequency.
            mode_num: Mode number.
            reordered_q_mat: Reordered normal coordinates.

        Returns:
            displaced_coords: Displaced coordinates.
        """
        h = self.step_modifier(self.stepsize, eq_freq)
        displaced_coords = np.zeros((len(self.molecule.labels), 3))
        for i in range(3):
            displaced_coords[:, i] += (self.molecule.coordinates[:, i]) + (reordered_q_mat[
                (mode_num*3) + i] * h) * (constants.bohr2angstroms) * factor
        return displaced_coords

    @staticmethod
    def step_modifier(stepsize: float, eq_freq: float) -> float:
        r"""Modify stepsize from units of reduced normal coordinates
        to units of normal coordinates.

        Normal coordinates Q are converted to reduced normal coordinates q:
        Q = :math:`(\hbar/ 2 \pi c \omega)^{0.5} q.`

        Args:
            stepsize: Step size in reduced normal coordinates.
            eq_freq: An equilibrium frequency.

        Returns:
            Modified stepsize in normal coordinates.
        """
        return stepsize * \
            vibav_constants.hbar_a**0.5 / \
            ((2 * np.pi * vibav_constants.c_cm * eq_freq * constants.amu2kg)**0.5 * constants.bohr2angstroms)

    def write_xyz_input(self, coords: np.ndarray, filename: str) -> None:
        """Write .xyz file for a given displaced geometry.

        Args:
            coords: Coordinates to write.
            filename: Name of the file to write.
        """
        with open(filename, 'w') as f:
            f.write(f'{len(self.molecule.labels)}\n')
            f.write('angstrom\n')
            for i in range(len(self.molecule.labels)):
                f.write(f'{self.molecule.labels[i]} {coords[i,0]} {coords[i,1]} {coords[i,2]}\n')

    def write_gau_input(self, coords: np.ndarray, filename: str) -> None:
        """Write .gau file for a given displaced geometry.

        Args:
            coords: Coordinates to write.
            filename: Name of the file to write.
        """
        with open(filename, 'w') as f:
            f.write(f'{len(self.molecule.labels)}' + '\n\n')
            f.write(f'{self.molecule.charge} {self.molecule.multiplicity}  \n')
            for i in range(len(self.molecule.labels)):
                f.write(f'{self.molecule.labels[i]} {coords[i,0]} {coords[i,1]} {coords[i,2]}\n')


class ComputeVibCorrection(NamedTuple):
    """Data structure for vibrational correction to properties."""
    property_first_derivatives: np.ndarray
    property_second_derivatives: np.ndarray
    mean_displacement_q: np.ndarray
    mean_displacement_q2: np.ndarray
    cubic_force_constants: np.ndarray
    vibrational_corrections: np.ndarray


class ComputeVibAvCorrection:
    """ComputeVibAvCorrection class for computing a vibrational correction
       to a property."""

    def __init__(self, hess_objects: Sequence[OutputParser], prop_objects: Sequence[OutputParser],
                 vibav_settings: VibAvSettings) -> None:
        """Initialize ComputeVibAvCorrection instance.

        Args:
            hess_objects: Hessian property objects.
            prop_objects: property objects.
            vibav_settings: VibAvSettings class object.
        """
        self.hess_objects = hess_objects
        self.prop_objects = prop_objects
        self.vibav_settings = vibav_settings
        self.call_vibav()

    def call_vibav(self) -> None:
        """Compute vibrational correction to properties."""
        q_mat_displaced = self.displace_q_geometries()
        factor = self.conversion_factor()
        self.cfc = self.cubic_fc(q_mat_displaced, factor)
        self.process_property_parsers()
        self.property_first_derivatives = np.zeros(
            (len(self.vibav_settings.eq_freqs), self.property_mat[0].shape[-1]))
        self.property_second_derivatives = np.zeros(
            (len(self.vibav_settings.eq_freqs), self.property_mat[0].shape[-1]))
        self.vibrational_corrections = np.zeros((self.property_mat[0].shape[-1]))
        labels = [self.vibav_settings.property_str, self.property_labels]
        if 'DSO Corr.' in self.vibav_settings.property_str:
            total_corrections_matrix = np.zeros((self.property_mat[0].shape[-2], self.property_mat[0].shape[-1]))
            for i in range(self.property_mat[0].shape[-2]):
                updated_labels = [labels[0][i], labels[1][:]]
                correction_terms = self.compute_vib_correction(self.property_mat[:, i, :], updated_labels)
                total_corrections_matrix[i] = correction_terms.vibrational_corrections
                self.property_first_derivatives += correction_terms.property_first_derivatives
                self.property_second_derivatives += correction_terms.property_second_derivatives
                self.vibrational_corrections += correction_terms.vibrational_corrections
            self.write_output_file(total_corrections_matrix)
        elif self.vibav_settings.property_str in ['Polarizability', 'Opt. Rotation']:
            self.property_first_derivatives = np.zeros(
                (self.property_mat.shape[1], len(self.vibav_settings.eq_freqs)))
            self.property_second_derivatives = np.zeros(
                (self.property_mat.shape[1], len(self.vibav_settings.eq_freqs)))
            self.vibrational_corrections = np.zeros((self.property_mat.shape[1]))
            for i in range(self.property_mat.shape[-2]):
                updated_labels = [labels[0], labels[1][i]]
                correction_terms = self.compute_vib_correction(self.property_mat[:, i, :], updated_labels)
                self.property_first_derivatives[i] = correction_terms.property_first_derivatives.reshape(
                    len(self.vibav_settings.eq_freqs))
                self.property_second_derivatives[i] = correction_terms.property_second_derivatives.reshape(
                    len(self.vibav_settings.eq_freqs))
                self.vibrational_corrections[i] = correction_terms.vibrational_corrections
            self.write_output_file(self.vibrational_corrections)
        else:
            correction_terms = self.compute_vib_correction(self.property_mat, labels)
            self.property_first_derivatives = correction_terms.property_first_derivatives
            self.property_second_derivatives = correction_terms.property_second_derivatives
            self.vibrational_corrections = correction_terms.vibrational_corrections
            self.write_output_file(self.vibrational_corrections)
        self.cubic_force_constants = correction_terms.cubic_force_constants
        self.mean_displacement_q = correction_terms.mean_displacement_q
        self.mean_displacement_q2 = correction_terms.mean_displacement_q2

    def compute_vib_correction(self, property_mat: np.ndarray,
                               property_labels: Sequence[str | Sequence[str]]) -> ComputeVibCorrection:
        r"""Vibrational correction to properties.

        Args:
            property_mat: property matrix.
            property_labels: Property labels.

        Returns:
            ComputeVibCorrection class object.

        The vibrational correction to a property P is then given by
        :math:`\Delta^\text{VPT2}P = -\frac{1}{4}\sum_i\frac{1}{\omega_i}\frac{\partial P}{\partial q_i} \
        \sum_j k_{ijj} + \frac{1}{4}\sum_{i} \frac{\partial^2 P}{\partial {q_i^2}}`.
        Temperature dependent corrections to properties are calculated using
        :math:`\Delta^\text{VPT2}P = \
        -\frac{1}{4}\sum_i\frac{1}{\omega_i}\frac{\partial P}{\partial q_i} \
        \left(\sum_j k_{ijj}\coth\left(\frac{{h}c\omega_j}{2kT}\right)\right) + \
        \frac{1}{4}\sum_{i}\frac{\partial^2 P}{\partial q_i^2}\coth\left(\frac{hc\omega_i}{2kT}\right)`
        """
        num_modes = len(self.vibav_settings.eq_freqs)
        if self.vibav_settings.fitting_order == 3 and self.vibav_settings.differentiation_method == 'linear':
            motherlist_property = np.array(property_mat[:-1]).reshape(num_modes, property_mat.shape[-1] * 2)
        else:
            motherlist_property = np.array(property_mat[:-1]).reshape(num_modes, property_mat.shape[-1] * 4)
        max_degree_derivative = 2
        if self.vibav_settings.differentiation_method == 'linear':
            diff1, diff2 = linear_property(motherlist_property, property_mat[-1], self.vibav_settings.stepsize,
                                           self.vibav_settings.fitting_order)
            diff1 = np.array(diff1).reshape(len(self.vibav_settings.eq_freqs), property_mat.shape[1])
            diff2 = np.array(diff2).reshape(len(self.vibav_settings.eq_freqs), property_mat.shape[1])
        else:
            dcoefs = polynomial_property(motherlist_property, property_mat[-1], self.vibav_settings.stepsize,
                                         self.vibav_settings.fitting_order, max_degree_derivative,
                                         self.vibav_settings.plot_polyfittings, property_labels)
            derivatives_mat = np.zeros((len(dcoefs), max_degree_derivative))
            for i in range(len(dcoefs)):
                for j in range(len(dcoefs[i].T)):
                    derivatives_mat[i, j] = poly.polyval(0, dcoefs[i, j])
            diff1 = np.array(derivatives_mat[:, 0]).reshape(len(self.vibav_settings.eq_freqs),
                                                            property_mat.shape[1])
            diff2 = np.array(derivatives_mat[:, 1]).reshape(len(self.vibav_settings.eq_freqs),
                                                            property_mat.shape[1])
        q, q2 = self.mean_displacement()
        if abs(self.vibav_settings.temperature) < 10**-12:
            correction = np.einsum('ri,r->i', diff1, q) + 0.5 * np.einsum('ri,r->i', diff2, q2)
        else:
            const = constants.h * vibav_constants.c_cm / (2 * constants.kb * self.vibav_settings.temperature)
            correction = np.einsum('ri,r->i', diff1,
                                   q) + 0.5 * np.einsum('ri,r,r->i', diff2, q2,
                                                        np.tanh(const * self.vibav_settings.eq_freqs)**(-1))
        return ComputeVibCorrection(property_first_derivatives=diff1,
                                    property_second_derivatives=diff2,
                                    mean_displacement_q=q,
                                    mean_displacement_q2=q2,
                                    cubic_force_constants=self.cfc,
                                    vibrational_corrections=correction)

    def mean_displacement(self) -> tuple[np.ndarray, np.ndarray]:
        r"""Calculate mean displacements.

        Returns:
            q: Mean displacement (dimensionless).
            q2: Mean displacement squared (dimensionless).

        Mean displacement is defined as:
        :math:`\left<\phi^{(1)}\left|q_i\right|\phi^{(0)}\right> = \frac{1}{4}\sum_i\frac{1}{\omega_i}\sum_j k_{ijj}`
        Mean displacement squared is defined as:
        :math:`\left<\phi^{(0)}\left|{q_i}{q_j}\right|\phi^{(0)}\right> = \frac{1}{2}`
        """
        if abs(self.vibav_settings.temperature) < 10**-12:
            q = -0.25 * np.einsum('r,rs->r', self.vibav_settings.eq_freqs**(-1), self.cfc)
        else:
            const = constants.h * vibav_constants.c_cm / (2 * constants.kb * self.vibav_settings.temperature)
            const2 = 0.5 * constants.kb * self.vibav_settings.temperature * (1 / constants.amu2kg) / (1e8) / (
                2 * np.pi * vibav_constants.c_cm) * (constants.h * vibav_constants.c_cm)**(-1 / 2)
            q = -0.25 * np.einsum('r,rs,s->r', self.vibav_settings.eq_freqs**(-1), self.cfc,
                                  np.tanh(const * self.vibav_settings.eq_freqs)**(-1)) \
                - np.einsum('r,r->r', const2 * self.vibav_settings.eq_freqs**(-1 / 2), self.rotational_contribution())
            q2 = 0.5 * np.tanh(const * self.vibav_settings.eq_freqs)**(-1)
        q2 = np.repeat(0.5, len(self.vibav_settings.eq_freqs))
        return q, q2

    def rotational_contribution(self) -> np.ndarray:
        r"""Calculate rotational contribution to a temperature dependent vibrational correction.

        Returns:
            quotient: Rotational contribution to vibrational correction in ang^(-1)*amu^(-1/2).
        """
        sym_analysis = SymmetryAnalysis(self.vibav_settings.molecule.elements,
                                        self.vibav_settings.molecule.coordinates)
        eig_vals, _ = np.linalg.eigh(sym_analysis._calculate_moment_of_inertia_tensor())
        quotient = np.zeros_like(self.vibav_settings.eq_freqs)
        for idx in range(len(self.vibav_settings.eq_freqs)):
            diag_coeffs = np.diag(self.coefficients_of_inertia_expansion(self.transformation_mat[:, idx]))
            quotient[idx] = np.sum(diag_coeffs / eig_vals)
        return quotient

    def coefficients_of_inertia_expansion(self, mode_vector) -> np.ndarray:
        """Calculate coefficients of inertia expansion.

        Args:
            mode_vector: Normal coordinate vector.

        Returns:
            coeff_expansion_tensor: Coefficients of inertia expansion in ang*amu^(1/2).
        """
        tensor = np.zeros((3, 3))
        amass = self.vibav_settings.molecule.masses
        coords = self.vibav_settings.molecule.coordinates
        num_atoms = self.vibav_settings.molecule.num_atoms
        vector = np.reshape(mode_vector, (num_atoms, 3))
        tensor[0, 0] = 2 * np.sum((amass)**0.5 * ((coords[:, 1] * vector[:, 1]) + (coords[:, 2] * vector[:, 2])))
        tensor[1, 1] = 2 * np.sum((amass)**0.5 * ((coords[:, 0] * vector[:, 0]) + (coords[:, 2] * vector[:, 2])))
        tensor[2, 2] = 2 * np.sum((amass)**0.5 * ((coords[:, 0] * vector[:, 0]) + (coords[:, 1] * vector[:, 1])))
        tensor[0, 1] = tensor[1, 0] = -2 * np.sum((amass)**0.5 * (coords[:, 0] * vector[:, 1]))
        tensor[0, 2] = tensor[2, 0] = -2 * np.sum((amass)**0.5 * (coords[:, 0] * vector[:, 2]))
        tensor[1, 2] = tensor[2, 1] = -2 * np.sum((amass)**0.5 * (coords[:, 1] * vector[:, 2]))
        return tensor

    def cubic_fc(self, q_mat_displaced: np.ndarray, factor: np.ndarray) -> np.ndarray:
        r"""Calculate cubic force constants numerically.

        Args:
            q_mat_displaced: Displaced normal coordinate matrix.
            factor: Conversion factor in units of bohr^-2*amu^-1.

        Returns:
            Cubic force constants in units of cm^-1.

        Cubic force constants are calculated numerically using the following equation:
        :math:`\Phi_{ijk} \simeq \
        \frac{1}{3}\left(\frac{\Phi_{jk}(\partial Q_i)-\Phi_{jk}(\partial Q_i)}{2\partial Q_i} \
        + \frac{\Phi_{ki}(\partial Q_j)-\Phi_{ki}(\partial Q_j)}{2\partial Q_j} \
        + \frac{\Phi_{ij}(\partial Q_k)-\Phi_{ij}(\partial Q_k)}{2\partial Q_k}\right)`.

        Ref:
        Barone, V. J. Phys. Chem. 2005, 122, 014108
        """
        m_mat = diagonal_mass_matrix(self.vibav_settings.molecule, self.vibav_settings.isotopes)
        displaced_hessians = np.array([results_hess.hessian for results_hess in self.hess_objects])
        phi = (self.transformation_mat.T @ (m_mat @ displaced_hessians[:-1] @ m_mat @ self.transformation_mat))
        phi_eq = (self.transformation_mat.T
                  @ (m_mat @ self.vibav_settings.hessian @ m_mat @ self.transformation_mat))
        dq = np.array([np.linalg.norm(a) for a in q_mat_displaced])
        self.phi = phi
        self.phi_eq = phi_eq
        self.dq = dq
        if self.vibav_settings.differentiation_method == 'linear':
            k_ijk = linear_cfcs(phi, dq, self.vibav_settings.fitting_order)
        else:
            k_ijk = polynomial_cfcs(phi, phi_eq, dq, self.vibav_settings.fitting_order)
        dim = phi.shape[1]
        k_ijk = k_ijk.reshape(dim, dim, dim) * constants.hartree2J
        k_ijj = np.einsum('ijj,i,j,j->ij', k_ijk, factor**(-1/2), factor**(-1/2), factor**(-1/2)) \
            / constants.h / vibav_constants.c_cm
        return k_ijj

    def displace_q_geometries(self) -> np.ndarray:
        """Generate displaced normal mode geometries.

        Returns:
            Displaced normal coordinate matrix.
        """
        self.transformation_mat = self.vibav_settings.eigen_solutions.transformation_matrix
        m_mat = diagonal_mass_matrix(self.vibav_settings.molecule, self.vibav_settings.isotopes)
        q_mat = self.transformation_mat.T @ m_mat
        x = np.zeros_like(q_mat)
        for a in range(len(self.vibav_settings.eq_freqs)):
            x[a, :] = q_mat[a, :] * VibAvSettings.step_modifier(self.vibav_settings.stepsize,
                                                                self.vibav_settings.eq_freqs[a])
        m_matm = np.linalg.inv(m_mat)
        q_mat_displaced = (self.transformation_mat.T @ (m_matm @ x.T)).T
        return q_mat_displaced

    def conversion_factor(self) -> np.ndarray:
        """Calculate conversion factor (bohr^-2*amu^-1) to convert cubic force constants to cm^-1.

        Returns:
            Conversion factor.
        """
        l_eq = self.vibav_settings.eigen_solutions.eigenvalues
        return 2 * np.pi * (constants.hartree2J * constants.amu2kg * l_eq)**0.5 / (constants.h * vibav_constants.m2au)

    def process_property_parsers(self) -> None:
        """Process property values to ensure correct dimensionality.
        """
        program = self.vibav_settings.property_program
        self.output_file_header = 'Vibrational Correction'
        if self.vibav_settings.property_str == 'Iso. Shieldings' and program in ('dalton', 'gaussian'):
            self.property_labels = self.vibav_settings.molecule.labels
            property_mat = np.array([results.nmr_shieldings for results in self.prop_objects])
        elif self.vibav_settings.property_str == 'Hyperfine Couplings' and program in ('gaussian'):
            self.property_labels = self.vibav_settings.molecule.labels
            property_mat = np.array([results.hyperfine_couplings for results in self.prop_objects])
        elif self.vibav_settings.property_str in ['Polarizability', 'Opt. Rotation'
                                                  ] and program in ('dalton', 'gaussian'):
            if self.vibav_settings.property_str == 'Polarizability':
                property_values = np.array([results.polarizabilities.values for results in self.prop_objects])
                self.property_labels = [
                    f'{round(float(result),4)}a.u' for result in self.prop_objects[0].polarizabilities.frequencies
                ]
            elif self.vibav_settings.property_str == 'Opt. Rotation':
                property_values = np.array([results.optical_rotations.values for results in self.prop_objects])
                self.property_labels = [
                    f'{round(float(result),4)}nm' for result in self.prop_objects[0].optical_rotations.frequencies
                ]
            property_mat = np.zeros((property_values.shape[0], property_values.shape[1], 1))
            for i in range(property_values.shape[0]):
                for j in range(property_values.shape[1]):
                    property_mat[i, j] = np.trace(property_values[i, j]) / 3
        elif self.vibav_settings.property_str[0] == 'DSO Corr.' and program in ('dalton', 'gaussian'):
            property_mat = np.array([results.spin_spin_couplings for results in self.prop_objects])
            self.property_labels = [results for results in self.prop_objects[0].spin_spin_labels]
        self.property_mat = property_mat

    def write_output_file(self, total_corrections: np.ndarray) -> None:
        """Write output file for vibrational correction results.

        Args:
            total_corrections: Property values.
        """
        if os.path.exists('vibav_results.yaml'):
            os.remove('vibav_results.yaml')
        if 'DSO Corr.' in self.vibav_settings.property_str:
            total_corrections = np.vstack((np.sum(total_corrections, axis=0), total_corrections))
            terms = ['Total'] + list(self.vibav_settings.property_str)
            for idx in range(len(total_corrections)):
                correction = total_corrections[idx, :].tolist()
                dict_vals = {
                    terms[idx]: {
                        f'{k} _{idx + 1}': v
                        for k, (idx, v) in zip(self.property_labels, enumerate(correction))
                    }
                }
                with open(r'vibav_results.yaml', 'a') as my_results:
                    yaml.dump(dict_vals, my_results, sort_keys=False)
        else:
            correction = total_corrections.tolist()
            dict_vals = {
                self.output_file_header: {
                    f'{k} _{idx + 1}': v
                    for k, (idx, v) in zip(self.property_labels, enumerate(correction))
                }
            }
            with open(r'vibav_results.yaml', 'a') as my_results:
                yaml.dump(dict_vals, my_results, sort_keys=False)
