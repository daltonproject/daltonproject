Development guide
=================

Here is a brief outline for the development goal in terms of code, readability,
and maintainability.


Code Style
----------

The code style is enforced using `pre-commit <https://pre-commit.com/>`_ hooks
that are run in the GitLab CI pipeline. It will check and format Python source
files using the following code-style checkers and formatters

- `Flake8 <https://flake8.pycqa.org/>`_
- `YAPF <https://github.com/google/yapf>`_
- `isort <https://timothycrosley.github.io/isort/>`_
- `mypy <http://mypy-lang.org/>`_
- `Doc8 <https://github.com/PyCQA/doc8>`_
- `pydocstyle <https://github.com/PyCQA/pydocstyle>`_

In addition, it will check and fix other file types for trailing whitespaces
and more. These code-style checkers and formatters are used to make the
code-base look uniform and check that it is PEP8 compliant. Note that the
line-length limit is set to 118 characters even though PEP8 recommends 79.

The pre-commit hooks are set up locally by running the following two commands
in the root of the Dalton Project directory::

    $ pip install pre-commit
    $ pre-commit install

During a **git commit**, if any pre-commit hook fails, mostly you will simply
need to **git add** the affected files and **git commit** again, because most
tools will automatically reformat the files. Staged files can also be checked
before committing by running::

    $ pre-commit run

Again, if a step fails the tools will in most cases also reformat the files
that caused the failure. You will therefore need to stage those files again
(**git add**) and they are then ready to **git commit**.

If you want to run pre-commit on all files this can be done with the following
command::

    $ pre-commit run --all-files


Continuous Integration and Code Coverage
----------------------------------------

The Dalton Project uses continuous integration through GitLab CI and code
coverage through `codecov.io <https://codecov.io/gl/daltonproject/daltonproject>`_.

The test suite for Dalton Project can be run locally using::

    $ pytest tests/*

The requirements for the testing suite can be installed by::

    $ pip install [--user] -r tests/requirements.txt

Documentation
-------------

The documentation is compiled using `sphinx <https://github.com/sphinx-doc/sphinx>`_.
The documentation is automatically hosted using `Read the Docs <https://readthedocs.org/>`_.
Note that **autodoc** is enabled for `Google-style docstrings <https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings>`_.

The documentation can be generated locally by running the following command::

    $ sphinx-build docs local_docs

Requirements for the documentation can be installed as::

    $ pip install [--user] -r docs/requirements.txt
