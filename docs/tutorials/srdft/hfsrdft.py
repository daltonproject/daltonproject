import daltonproject as dp

molecule, basis = dp.mol_reader('water.mol')
hfsrdft = dp.QCMethod('HFsrDFT', 'SRPBEGWS')
hfsrdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
hfsrdft_result = dp.dalton.compute(molecule, basis, hfsrdft, prop)
print('HFsrPBE energy =', hfsrdft_result.energy)
# HFsrPBE energy = -73.502309263107

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(hfsrdft_result.filename).name == '40142d8f864b1387147e3d4603629a45f0377402'
np.testing.assert_allclose(hfsrdft_result.energy, -73.502309263107)
