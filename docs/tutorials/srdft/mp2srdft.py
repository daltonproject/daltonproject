import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyze_symmetry()
mp2srdft = dp.QCMethod('MP2srDFT', 'SRPBEGWS')
mp2srdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
mp2srdft_result = dp.dalton.compute(molecule, basis, mp2srdft, prop)
print(mp2srdft_result.filename)
# 282603d082ffdea3ff8ef1432f5dfe1f69c12db7
print('MP2srPBE energy =', mp2srdft_result.energy)
# MP2srPBE energy = -244.7774549828

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(mp2srdft_result.filename).name == '282603d082ffdea3ff8ef1432f5dfe1f69c12db7'
np.testing.assert_allclose(mp2srdft_result.energy, -244.7774549828)
