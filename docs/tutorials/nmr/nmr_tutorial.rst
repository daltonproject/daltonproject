.. role:: python(code)
   :language: python

Calculation of NMR parameters
=============================

This tutorial explains how to calculate NMR parameters using the
Dalton Project platform.

*Note, that the quantum chemistry methods used in this tutorial may not be
suitable for the particular problem that you want to solve. Moreover, the basis
sets used here are minimal in order to allow you to progress fast through the
tutorial and should under no circumstances be used in production calculations.*


Shieldings
----------

NMR shieldings can be obtained with, for example, HF, CASSCF, or DFT.
To calculate shieldings, create a Property object where
:python:`nmr_shieldings` is set to :python:`True`. A minimal example is shown
below.

.. literalinclude:: simple_nmr.py
    :lines: 1-10
