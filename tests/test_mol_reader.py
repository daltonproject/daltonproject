from __future__ import annotations  # Delete when Python 3.10 is oldest supported version

from collections.abc import Sequence

import numpy as np
import pytest
from qcelemental import PhysicalConstantsContext

import daltonproject as dp

constants = PhysicalConstantsContext('CODATA2018')


def check_if_molecule_is_same(xyz_original: np.ndarray,
                              xyz_new: np.ndarray,
                              labels_original: Sequence[str],
                              labels_new: Sequence[str],
                              threshold: float = 1e-3) -> bool:
    """Checks if two molecules are identical within a threshold.

    Args:
      xyz_original: Molecule coordinates.
      xyz_new: Molecule coordinates.
      labels_original: Labels.
      labels_new: Labels.
      threshold: Threshold for molecules being identical.

    Returns:
      True, if the molecules are identical.
    """
    if len(xyz_original) != len(xyz_new):
        return False
    elif len(xyz_original) != len(labels_original):
        return False
    elif len(xyz_original) != len(labels_new):
        return False
    same_check = True
    atom_same_check = np.zeros(len(xyz_original))
    for i in range(len(xyz_original)):
        min_distance2 = 10**12
        for j in range(len(xyz_new)):
            if labels_original[i] != labels_new[j]:
                continue
            if atom_same_check[j] != 0:
                continue
            distance2 = np.sum((xyz_original[i] - xyz_new[j])**2)
            if distance2 < min_distance2:
                idx_closest = j
                min_distance2 = distance2
                if min_distance2**0.5 < threshold:
                    break
        if min_distance2**0.5 < threshold:
            atom_same_check[idx_closest] = 1
        else:
            same_check = False
            break
    return same_check


@pytest.mark.datafiles(
    pytest.DATADIR / 'MnO4.mol',
    pytest.DATADIR / 'MnO4_generator.mol',
)
def test_atombasis(datafiles):
    # Reference is converted to Angstrom to match output from daltonproject.
    ref_coordinates = np.array([
        [0.00000000, 0.00000000, 0.00000000],
        [1.7314309298043, 1.7314309298043, -1.7314309298043],
        [-1.7314309298043, 1.7314309298043, 1.7314309298043],
        [1.7314309298043, -1.7314309298043, 1.7314309298043],
        [-1.7314309298043, -1.7314309298043, -1.7314309298043],
    ]) * constants.conversion_factor('bohr', 'angstrom')
    ref_basis_set = {'Mn': 'aug-cc-pVTZ-J', 'O': 'aug-cc-pVTZ-J'}
    ref_charge = -1
    molecule, basis = dp.mol_reader(datafiles / 'MnO4.mol')
    np.testing.assert_allclose(molecule.coordinates, ref_coordinates)
    assert ref_charge == molecule.charge
    assert len(ref_basis_set) == len(basis.basis)
    for key in ref_basis_set:
        assert ref_basis_set[key] == basis.basis[key]
    molecule, basis = dp.mol_reader(datafiles / 'MnO4_generator.mol')
    np.testing.assert_allclose(molecule.coordinates, ref_coordinates)
    assert ref_charge == molecule.charge
    assert len(ref_basis_set) == len(basis.basis)
    for key in ref_basis_set:
        assert ref_basis_set[key] == basis.basis[key]


@pytest.mark.datafiles(
    pytest.DATADIR / 'Formaldehyde.mol',
    pytest.DATADIR / 'Formaldehyde_generator.mol',
)
def test_basis(datafiles):
    ref_coordinates = np.array([
        [0.0, 0.0, -1.13698349],
        [0.0, 0.0, 1.13429162],
        [0.0, 1.76355276, -2.23208171],
        [0.0, -1.76355276, -2.23208171],
    ]) * constants.conversion_factor('bohr', 'angstrom')
    ref_basis_set = 'aug-cc-pVTZ'
    ref_charge = 0
    molecule, basis = dp.mol_reader(datafiles / 'Formaldehyde.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert ref_basis_set == basis.basis
    molecule, basis = dp.mol_reader(datafiles / 'Formaldehyde_generator.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)


@pytest.mark.datafiles(
    pytest.DATADIR / 'Pagodane_Z_X_Y.mol',
    pytest.DATADIR / 'Pagodane_Y_Z_X.mol',
)
def test_noncanonical_d2h(datafiles):
    ref_basis_set = {'C': '3-21G', 'H': '3-21G'}
    ref_charge = 0
    molecule, basis = dp.mol_reader(datafiles / 'Pagodane_Z_X_Y.mol')
    molecule2, basis = dp.mol_reader(datafiles / 'Pagodane_Y_Z_X.mol')
    assert check_if_molecule_is_same(molecule.coordinates, molecule2.coordinates, molecule.labels,
                                     molecule2.labels)
    assert ref_charge == molecule.charge
    assert ref_basis_set == basis.basis


@pytest.mark.datafiles(
    pytest.DATADIR / 'Formaldehyde_letter_casing.mol',
    pytest.DATADIR / 'Formaldehyde_generator_letter_casing.mol',
)
def test_case_sensitivity(datafiles):
    ref_coordinates = np.array([
        [0.0, 0.0, -1.13698349],
        [0.0, 0.0, 1.13429162],
        [0.0, 1.76355276, -2.23208171],
        [0.0, -1.76355276, -2.23208171],
    ]) * constants.conversion_factor('bohr', 'angstrom')
    ref_basis_set = 'aug-cc-pVTZ'
    ref_charge = 0
    molecule, basis = dp.mol_reader(datafiles / 'Formaldehyde_letter_casing.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert ref_basis_set == basis.basis
    molecule, basis = dp.mol_reader(datafiles / 'Formaldehyde_generator_letter_casing.mol')
    assert molecule.coordinates == pytest.approx(ref_coordinates)


@pytest.mark.datafiles(
    pytest.DATADIR / 'Formaldehyde.mol',
    pytest.DATADIR / 'Formaldehyde_angstrom.mol',
    pytest.DATADIR / 'Formaldehyde_generator.mol',
    pytest.DATADIR / 'Formaldehyde_generator_angstrom.mol',
)
def test_specifying_units_molfile(datafiles):
    molecule_bohr, basis = dp.mol_reader(datafiles / 'Formaldehyde.mol')
    molecule_angstrom, basis = dp.mol_reader(datafiles / 'Formaldehyde_angstrom.mol')
    np.testing.assert_allclose(molecule_bohr.coordinates, molecule_angstrom.coordinates, atol=1e-5)
    molecule_bohr, basis = dp.mol_reader(datafiles / 'Formaldehyde_generator.mol')
    molecule_angstrom, basis = dp.mol_reader(datafiles / 'Formaldehyde_generator_angstrom.mol')
    np.testing.assert_allclose(molecule_bohr.coordinates, molecule_angstrom.coordinates, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'h2o_atombasis.mol', )
def test_atombasis_different_basis_same_atom(datafiles):
    molecule, basis = dp.mol_reader(datafiles / 'h2o_atombasis.mol')
    ref_coordinates = np.array([[0.39654857, 0., 0.23412541], [0., 0., -0.08749982], [-0.39654857, 0.,
                                                                                      0.23412541]])
    ref_charge = 0
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert '3-21G' == basis.basis['H1']
    assert 'STO-2G' == basis.basis['O']
    assert 'STO-3G' == basis.basis['H2']


@pytest.mark.datafiles(pytest.DATADIR / 'h3o_atombasis.mol', )
def test_atombasis_different_basis_same_atom_2(datafiles):
    molecule, basis = dp.mol_reader(datafiles / 'h3o_atombasis.mol')
    ref_coordinates = np.array([[0.39654857, 0., 0.23412541], [0., 0., -0.08749982], [-0.39654857, 0., 0.23412541],
                                [0.39654857, 0., 0.76330262]])
    ref_charge = 0
    assert molecule.coordinates == pytest.approx(ref_coordinates)
    assert ref_charge == molecule.charge
    assert '3-21G' == basis.basis['H1']
    assert 'STO-2G' == basis.basis['O']
    assert 'STO-3G' == basis.basis['H2']


@pytest.mark.parametrize('mol, atom_bas, charge, ref_coordinates', [
    ('o2-ano.mol', {'O': 'ANO-VT-TZ'}, 0, np.array([[0, 0, 0.6035265], [0, 0, -0.6035265]])),
    ('prop_alpha_aorpa.mol', 'cc-pVDZ', 0,
     np.array([[0, 0, 0.1241444240], [0, 1.43153, -0.985265576], [0, -1.43153, -0.985265576]], )
     * constants.conversion_factor('bohr', 'angstrom')),
    ('lih.mol', '4-31G', 0, np.array([[0, 0, 2.0969699107], [0, 0, -.9969699107]])),
    ('CH2O_3-21G.mol', '3-21G', 0, np.array([[0, 0, -2.28], [0, 0, 0], [1.7868, 0., 1.14], [-1.7868, 0., 1.14]])
     * constants.conversion_factor('bohr', 'angstrom')),
    ('H2O_3-21G_sym.mol', '3-21G', 0, np.array([[0, 0, 1], [0, 2.3736, -0.44317], [0, -2.3736, -0.44317]])
     * constants.conversion_factor('bohr', 'angstrom')),
])
@pytest.mark.datafiles(
    pytest.DATADIR / 'o2-ano.mol',
    pytest.DATADIR / 'prop_alpha_aorpa.mol',
    pytest.DATADIR / 'lih.mol',
    pytest.DATADIR / 'CH2O_3-21G.mol',
    pytest.DATADIR / 'H2O_3-21G_sym.mol',
)
def test_old_format(datafiles, mol, atom_bas, charge, ref_coordinates):
    molecule, basis = dp.mol_reader(datafiles / mol)
    assert basis.basis == atom_bas
    assert molecule.charge == charge
    assert molecule.coordinates == pytest.approx(ref_coordinates)
