import numpy as np
import pytest

import daltonproject as dp


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_set_lsdalton_state(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    dp.lsdalton.integrals.pylsdalton = True
    with pytest.raises(TypeError):
        dp.lsdalton.integrals._set_lsdalton_state(basis, basis, hf)
    with pytest.raises(TypeError):
        dp.lsdalton.integrals._set_lsdalton_state(water, water, hf)
    with pytest.raises(TypeError):
        dp.lsdalton.integrals._set_lsdalton_state(water, basis, basis)
    dp.lsdalton.integrals.pylsdalton = None


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_pylsdalton_exceptions(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    density = np.zeros(shape=(2, 2))
    hamiltonian = np.zeros(shape=(2, 2))
    metric = np.zeros(shape=(2, 2))
    specs = 'CRRRR'
    dp.lsdalton.integrals.pylsdalton = None
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.num_atoms(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.num_electrons(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.num_basis_functions(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.num_ri_basis_functions(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.overlap_matrix(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.kinetic_energy_matrix(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.nuclear_electron_attraction_matrix(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.coulomb_matrix(density, water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.exchange_matrix(density, water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.exchange_correlation(density, water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.fock_matrix(density, water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.eri(specs, water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.eri4(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.ri3(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.ri2(water, basis, hf)
    with pytest.raises(Exception, match='PyLSDalton is not available.'):
        dp.lsdalton.diagonal_density(hamiltonian, metric, water, basis, hf)
