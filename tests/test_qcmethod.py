import pytest

import daltonproject as dp


def test_qcmethod_hf():
    test = dp.QCMethod('hf')
    assert isinstance(test.settings, dict)
    assert test.settings['qc_method'] == 'HF'


def test_qcmethod_scf_hs():
    test = dp.QCMethod('hf')
    test.scf_occupation([3, 1, 1, 0])
    assert test.settings['doubly_occupied_orbitals'] == [3, 1, 1, 0]
    test.scf_occupation([3, 1, 1, 0], [0, 0, 0, 1])
    assert test.settings['doubly_occupied_orbitals'] == [3, 1, 1, 0]
    assert test.settings['singly_occupied_orbitals'] == [0, 0, 0, 1]
    with pytest.raises(TypeError, match='Occupation numbers must be given as a list of integers.'):
        test.scf_occupation('3, 1, 1, 0'), '0, 0, 0 ,1'
    with pytest.raises(ValueError, match='Number of occupation numbers must be equal.'):
        test.scf_occupation([3, 1, 1, 0], [0, 0, 0])


def test_qcmethod_dft():
    test = dp.QCMethod(qc_method='DFT', xc_functional='b3lyp')
    assert isinstance(test.settings, dict)
    assert test.settings['qc_method'] == 'DFT'
    assert test.settings['xc_functional'] == 'B3LYP'
    test = dp.QCMethod(qc_method='DFT')
    test.xc_functional('B3LYP')
    assert test.settings['qc_method'] == 'DFT'
    assert test.settings['xc_functional'] == 'B3LYP'


def test_qcmethod_dft_df_admm():
    test = dp.QCMethod(qc_method='DFT', xc_functional='b3lyp', coulomb='DF', exchange='ADMM')
    assert isinstance(test.settings, dict)
    assert test.settings['qc_method'] == 'DFT'
    assert test.settings['xc_functional'] == 'B3LYP'
    assert test.settings['coulomb'] == 'DF'
    assert test.settings['exchange'] == 'ADMM'
    assert test.settings['xc_functional'] == 'B3LYP'


def test_qcmethod_exception():
    with pytest.raises(TypeError, match='QC method must be given as a string.'):
        dp.QCMethod(['HF'])
    with pytest.raises(TypeError, match='XC functional must be given as a string.'):
        dp.QCMethod(qc_method='DFT', xc_functional=['B3LYP'])
    with pytest.raises(TypeError, match='Specifying XC functional is only valid together with DFT methods.'):
        dp.QCMethod(qc_method='HF', xc_functional='B3LYP')
    with pytest.raises(TypeError):
        dp.QCMethod()
    with pytest.raises(TypeError, match='SCF threshold must be given as a float.'):
        test = dp.QCMethod('HF')
        test.scf_threshold('1e-6')
    with pytest.raises(TypeError, match='Specifying XC functional is only valid together with DFT methods.'):
        hf = dp.QCMethod(qc_method='HF')
        hf.xc_functional('B3LYP')


def test_qcmethod_scf_threshold():
    test = dp.QCMethod('HF')
    test.scf_threshold(1e-6)
    assert test.settings['scf_threshold'] == pytest.approx(1e-6)


def test_qcmethod_range_separation_parameter():
    with pytest.raises(TypeError, match='mu, the range separation parameter, must be given as a float.'):
        test = dp.QCMethod('HFsrDFT')
        test.range_separation_parameter('0.4')
    with pytest.raises(TypeError,
                       match='Specifying range separation parameter is only valid for range separated methods.'):
        test = dp.QCMethod('HF')
        test.range_separation_parameter(0.4)
    test = dp.QCMethod('HFsrDFT')
    test.range_separation_parameter(0.4)
    assert test.settings['range_separation_parameter'] == pytest.approx(0.4)
    test = dp.QCMethod('HFsrDFT')
    test.range_separation_parameter(1)
    assert test.settings['range_separation_parameter'] == pytest.approx(1.0)


def test_qcmethod_complete_active_space():
    with pytest.raises(ValueError, match='Too many active electrons for the number of CAS orbitals.'):
        test = dp.QCMethod('CASSCF')
        test.complete_active_space(6, 2, 1)
    with pytest.raises(TypeError, match='num_inactive_orbitals must only be integers.'):
        test = dp.QCMethod('CASSCF')
        test.complete_active_space(2, 4, '3')
    with pytest.raises(TypeError, match='num_cas_orbitals must only be integers.'):
        test = dp.QCMethod('CASSCF')
        test.complete_active_space(2, '4', 3)
    with pytest.raises(TypeError, match='num_active_electrons must be given as an integer.'):
        test = dp.QCMethod('CASSCF')
        test.complete_active_space(2.1, 4, 3)
    with pytest.raises(ValueError,
                       match='Dimension of num_inactive_orbitals and num_cas_orbitals must be the same.'):
        test = dp.QCMethod('CASSCF')
        test.complete_active_space(4, [4, 1, 1], [3, 1])
    with pytest.raises(TypeError, match='Specifying CAS is only valid for range multi-configurational methods.'):
        test = dp.QCMethod('HF')
        test.complete_active_space(2, 4, 3)
    test = dp.QCMethod('CASSCF')
    test.complete_active_space(2, 4, 3)
    assert test.settings['num_inactive_orbitals'] == [3]
    assert test.settings['num_cas_orbitals'] == [4]
    assert test.settings['num_active_electrons'] == 2


def test_input_orbital_coefficients_exceptions():
    casscf = dp.QCMethod('CASSCF')
    with pytest.raises(TypeError, match='orbitals must be an array or multiple arrays in a dictionary.'):
        casscf.input_orbital_coefficients(1.0)


def test_target_state():
    test = dp.QCMethod('CASSCF')
    test.target_state(3)
    assert test.settings['state_number'] == 3
    assert test.settings['state_symmetry'] == 1
    test.target_state(2, symmetry=3)
    assert test.settings['state_number'] == 2
    assert test.settings['state_symmetry'] == 3
    with pytest.raises(TypeError, match='state must be specified as an integer.'):
        test.target_state(3.4)
    with pytest.raises(TypeError, match='symmetry must be specified as an integer.'):
        test.target_state(2, symmetry=2.1)
    with pytest.raises(TypeError, match='Specifying target state is only valid for state-specific methods.'):
        test = dp.QCMethod('HF')
        test.target_state(3)


def test_multiplicity():
    test = dp.QCMethod('CASSCF')
    test.target_state(1, multiplicity=3)
    assert test.settings['multiplicity'] == 3


def test_exact_exchange():
    test = dp.QCMethod('HFSRDFT', 'SRXLDA SRCLDA')
    test.exact_exchange(0.2)
    assert test.settings['exact_exchange'] == 0.2
    with pytest.raises(TypeError, match='Exact exchange amount must be specified as a float'):
        test.exact_exchange('0.2')
    with pytest.raises(TypeError, match='Exact exchange amount must be specified as a float'):
        test.exact_exchange(1)
    with pytest.raises(ValueError, match='Exact exchange amount must be between zero and one'):
        test.exact_exchange(-0.1)
    with pytest.raises(ValueError, match='Exact exchange amount must be between zero and one'):
        test.exact_exchange(2.3)
    test = dp.QCMethod('HF')
    with pytest.raises(TypeError, match='Specifying Hartree-Fock-like exchange is only valid for DFT methods'):
        test.exact_exchange(0.2)


def test_qcmethod_repr():
    qc_method = dp.QCMethod(
        qc_method='DFT',
        xc_functional='B3LYP',
        coulomb='DF',
        exchange='ADMM',
    )
    ref_repr = """QCMethod(qc_method: DFT,
         xc_functional: B3LYP,
         coulomb: DF,
         exchange: ADMM)"""
    assert repr(qc_method) == ref_repr
    qc_method = dp.QCMethod(qc_method='HF')
    ref_repr = 'QCMethod(qc_method: HF)'
    assert repr(qc_method) == ref_repr
