import shutil

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz')
def test_compute_exceptions(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    energy = dp.Property(energy=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with pytest.raises(TypeError):
        dp.gaussian.compute(water, basis, basis, energy)
    with pytest.raises(TypeError):
        dp.gaussian.compute(water, hf, hf, energy)
    with pytest.raises(TypeError):
        dp.gaussian.compute(hf, basis, hf, energy)
    with pytest.raises(TypeError):
        dp.gaussian.compute(water, basis, hf, hf)
    if not shutil.which(cmd='g16'):
        with pytest.raises(FileNotFoundError, match='The g16 script was not found or is not executable.'):
            dp.gaussian.compute(water, basis, hf, energy)


@pytest.mark.datafiles(
    pytest.DATADIR / 'water.gau',
    pytest.DATADIR / 'water_hessian_gaus.fchk',
    pytest.DATADIR / 'water_hessian_gaus.out',
)
def test_no_rerun(datafiles):
    """Testing that a calculation will not rerun if the output file exist."""
    with chdir(datafiles):
        output = dp.gaussian.compute(dp.Molecule(input_file=(datafiles / 'water.gau')),
                                     dp.Basis(basis='3-21G'),
                                     dp.QCMethod('HF'),
                                     dp.Property(energy=True),
                                     filename=f'{datafiles}/water_hessian_gaus')
    assert output.energy == pytest.approx(-7.488017437841883E+01)


@pytest.mark.datafiles(pytest.DATADIR / 'water_hessian_gaus.fchk')
def test_energy(datafiles):
    ref_energy = -74.88017437841883
    output = dp.gaussian.OutputParser(datafiles / 'water_hessian_gaus')
    np.testing.assert_allclose(output.energy, ref_energy)


@pytest.mark.datafiles(pytest.DATADIR / 'water_hessian_gaus.fchk')
def test_hessian(datafiles):
    ref_hessian = np.array('''
1.13060550e+00  3.61553670e-16  2.59167787e-16
-5.65302752e-01 -4.05239856e-16 -4.44713504e-01
-5.65302752e-01  4.36861857e-17 4.44713504e-01
3.61553670e-16 -7.70744197e-02  2.83290336e-16
-5.18585666e-16 3.85372098e-02 -2.35410722e-16
1.57031996e-16  3.85372098e-02 -4.78796132e-17
2.59167787e-16  2.83290336e-16  6.18400950e-01
-5.23107025e-01 -6.28309756e-16 -3.09200475e-01
5.23107025e-01  3.45019420e-16 -3.09200475e-01
-5.65302752e-01 -5.18585666e-16 -5.23107025e-01
5.63933199e-01 2.98508738e-16  4.83910265e-01
1.36955306e-03  2.20076929e-16 3.91967605e-02
-4.05239856e-16  3.85372098e-02 -6.28309756e-16
2.98508738e-16 -1.31445662e-01  6.80890939e-16
1.06731118e-16  9.29084523e-02 -5.25811832e-17
-4.44713504e-01 -2.35410722e-16 -3.09200475e-01
4.83910265e-01 6.80890939e-16  5.31797988e-01
-3.91967605e-02 -4.45480217e-16 -2.22597513e-01
-5.65302752e-01  1.57031996e-16  5.23107025e-01
1.36955306e-03 1.06731118e-16 -3.91967605e-02
5.63933199e-01 -2.63763114e-16 -4.83910265e-01
4.36861857e-17  3.85372098e-02  3.45019420e-16
2.20076929e-16 9.29084523e-02 -4.45480217e-16
-2.63763114e-16 -1.31445662e-01 1.00460796e-16
4.44713504e-01 -4.78796132e-17 -3.09200475e-01
3.91967605e-02 -5.25811832e-17 -2.22597513e-01
-4.83910265e-01  1.00460796e-16 5.31797988e-01'''.split()).astype(np.float64).reshape(9, 9)
    output = dp.gaussian.OutputParser(datafiles / 'water_hessian_gaus')
    np.testing.assert_allclose(output.hessian, ref_hessian)


@pytest.mark.datafiles(pytest.DATADIR / 'water_shieldings_gaus.fchk')
def test_nmr_shieldings(datafiles):
    filename = (datafiles / 'water_shieldings_gaus')
    result = dp.gaussian.OutputParser(filename)
    np.testing.assert_allclose(np.array([411.91153286, 36.25535837, 36.25535837]), result.nmr_shieldings)


@pytest.mark.datafiles(pytest.DATADIR / 'nh3_final_geometry.out', )
def test_gaussian_final_geometry(datafiles):
    output = dp.gaussian.OutputParser(datafiles / 'nh3_final_geometry')
    reference = np.array([[-0.071457, -0.000000, 0.000000], [0.354512, -0.940569, 0.000000],
                          [0.354512, 0.470285, -0.814544], [0.354512, 0.470285, 0.814544]])
    geom = output.final_geometry
    np.testing.assert_allclose(reference, geom)


@pytest.mark.datafiles(pytest.DATADIR / 'spin_spin_couplings_gaus.out')
def test_spin_spin_couplings(datafiles):
    filename = (datafiles / 'spin_spin_couplings_gaus')
    result = dp.gaussian.OutputParser(filename)
    ref_spin_spin_couplings = np.array([[-0.734511, -0.112748, 0.727237], [-9.86943, -1.51503, -0.0481305],
                                        [0.579602, 0.0889934, 0.34042], [18.3338, 2.81461, -4.81412]])
    np.testing.assert_allclose(ref_spin_spin_couplings, result.spin_spin_couplings)


@pytest.mark.datafiles(pytest.DATADIR / 'spin_spin_labels_gaus.out')
def test_spin_spin_labels(datafiles):
    filename = (datafiles / 'spin_spin_labels_gaus')
    result = dp.gaussian.OutputParser(filename)
    assert result.spin_spin_labels == ['H N', 'H N', 'H H', 'H N', 'H H', 'H H']


@pytest.mark.datafiles(pytest.DATADIR / 'polarizability_gaus.fchk')
def test_polarizabilities(datafiles):
    filename = (datafiles / 'polarizability_gaus')
    result = dp.gaussian.OutputParser(filename)
    ref_polar_tensor = np.array('''
 4.19277923E-02 -3.40892567E-17 -8.27805649E-16
 -3.40892567E-17  5.21834097E+00 -5.99520433E-15
-8.27805649E-16 -5.99520433E-15  2.13230898E+00
 4.38095518E-02 -2.77966336E-17 -9.73777026E-16
-2.77966336E-17  5.30131090E+00 -4.77395901E-15
 -9.73777026E-16 -4.77395901E-15  2.16135254E+00'''.split()).astype(np.float64).reshape(2, 3, 3)
    ref_frequencies = [0, 0.1]
    np.testing.assert_allclose(result.polarizabilities.values, ref_polar_tensor)
    np.testing.assert_allclose(result.polarizabilities.frequencies, ref_frequencies)


@pytest.mark.datafiles(pytest.DATADIR / 'single_polarizability_gaus.fchk')
def test_single_polarizability(datafiles):
    filename = (datafiles / 'single_polarizability_gaus')
    result = dp.gaussian.OutputParser(filename)
    ref_polar_tensor = np.array('''
3.619407e+00 -7.226005e-17 -6.439294e-15
-7.226005e-17 1.836850e-02 5.841082e-16
-6.439294e-15 5.841082e-16 2.903982e+00
'''.split()).astype(np.float64).reshape(1, 3, 3)
    ref_frequencies = [0]
    np.testing.assert_allclose(result.polarizabilities.values, ref_polar_tensor, atol=1e-5)
    np.testing.assert_allclose(result.polarizabilities.frequencies, ref_frequencies, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'hyperfine_couplings_gaus.fchk', )
def test_gaussian_hyperfine_couplings(datafiles):
    filename = (datafiles / 'hyperfine_couplings_gaus')
    result = dp.gaussian.OutputParser(filename)
    ref_hyperfine_couplings = np.array(
        [0.29013806, 0.11624592, 0.1167144, 0.1167144, 0.07018242, 0.07018242, 0.07007289])
    np.testing.assert_allclose(result.hyperfine_couplings, ref_hyperfine_couplings, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'optical_rotations_gaus.fchk')
def test_optical_rotations(datafiles):
    filename = (datafiles / 'optical_rotations_gaus')
    result = dp.gaussian.OutputParser(filename)
    ref_optical_rotations = np.array('''
  -0.0000000  0.303531169 -0.0000000
   1.42326341 -0.0000000  -0.0000000
  -0.0000000  -0.0000000  -0.0000000
  0.0000000  0.0636316307 0.0000000
  0.0318873104 -0.0000000  -0.0000000
  0.0000000  0.0000000  0.0000000 '''.split()).astype(np.float64).reshape(2, 3, 3)
    ref_frequencies = [0.5, 1.0]
    np.testing.assert_allclose(result.optical_rotations.values, ref_optical_rotations, atol=1e-5)
    np.testing.assert_allclose(result.optical_rotations.frequencies, ref_frequencies, atol=1e-5)


@pytest.mark.datafiles(pytest.DATADIR / 'empty_file.out', )
def test_output_parser_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.gaussian.OutputParser(filename)
    with pytest.raises(RuntimeError, match='Geometry optimization failed to converge to a minimum.'):
        output.final_geometry
