import pytest

import daltonproject as dp


def test_property_init():
    properties = dp.Property(
        energy=True,
        gradients=True,
        hessian=True,
        geometry_optimization=True,
        transition_state=True,
        excitation_energies=True,
    )
    assert 'energy' in properties.settings
    assert properties.settings['energy']
    assert 'transition_state' in properties.settings
    assert properties.settings['transition_state']
    assert 'gradients' in properties.settings
    assert properties.settings['gradients'] == 'analytic'
    assert 'hessian' in properties.settings
    assert properties.settings['hessian'] == 'analytic'
    assert 'geometry_optimization' in properties.settings
    assert properties.settings['geometry_optimization'] == 'BFGS'
    assert 'excitation_energies' in properties.settings
    assert properties.settings['excitation_energies'] == [5]
    properties = dp.Property(gradients={'method': 'analytic'})
    assert 'gradients' in properties.settings
    assert properties.settings['gradients'] == 'analytic'
    properties = dp.Property(hessian={'method': 'analytic'})
    assert 'hessian' in properties.settings
    assert properties.settings['hessian'] == 'analytic'
    properties = dp.Property(geometry_optimization={'method': 'BFGS'})
    assert 'geometry_optimization' in properties.settings
    assert properties.settings['geometry_optimization'] == 'BFGS'
    properties = dp.Property(excitation_energies={'states': 4, 'triplet': True})
    assert 'excitation_energies' in properties.settings
    assert properties.settings['excitation_energies'] == [4]
    assert 'excitation_energies_triplet' in properties.settings
    assert properties.settings['excitation_energies_triplet'] is True
    properties = dp.Property(two_photon_absorption={'states': 4})
    assert 'two_photon_absorption' in properties.settings
    assert properties.settings['two_photon_absorption'] == [4]
    properties = dp.Property(polarizability_gradients={'frequencies': [0.0, 0.01]})
    assert 'polarizability_gradients' in properties.settings
    assert properties.settings['polarizability_gradients'] is True
    assert 'polarizability_gradients_frequencies' in properties.settings
    assert properties.settings['polarizability_gradients_frequencies'] == [0.0, 0.01]


def test_property_energy():
    properties = dp.Property()
    properties.energy()
    assert 'energy' in properties.settings
    assert properties.settings['energy']


def test_property_gradients():
    properties = dp.Property()
    properties.gradients(method='numeric')
    assert 'gradients' in properties.settings
    assert properties.settings['gradients'] == 'numeric'


def test_property_hessian():
    properties = dp.Property()
    properties.hessian(method='numeric')
    assert 'hessian' in properties.settings
    assert properties.settings['hessian'] == 'numeric'


def test_property_geometry_optimization():
    properties = dp.Property()
    properties.geometry_optimization(method='DFP')
    assert 'geometry_optimization' in properties.settings
    assert properties.settings['geometry_optimization'] == 'DFP'


def test_transition_state():
    properties = dp.Property()
    properties.transition_state()
    assert 'transition_state' in properties.settings
    assert properties.settings['transition_state']


def test_property_excitation_energies():
    properties = dp.Property()
    properties.excitation_energies(states=2, triplet=True)
    assert 'excitation_energies' in properties.settings
    assert properties.settings['excitation_energies'] == [2]
    assert properties.settings['excitation_energies_triplet']
    properties.excitation_energies(states=[1, 2])
    assert properties.settings['excitation_energies'] == [1, 2]


def test_property_two_photon_absorption():
    properties = dp.Property()
    properties.two_photon_absorption(states=2)
    assert 'two_photon_absorption' in properties.settings
    assert properties.settings['two_photon_absorption'] == [2]
    properties.two_photon_absorption(states=[1, 2])
    assert properties.settings['two_photon_absorption'] == [1, 2]


def test_hyperfine_couplings():
    properties = dp.Property()
    properties.hyperfine_couplings(atoms=[1])
    assert 'hyperfine_couplings' in properties.settings


def test_spin_spin_couplings():
    properties = dp.Property()
    properties.spin_spin_couplings()
    assert 'spin_spin_couplings' in properties.settings


def test_dipole_gradients():
    properties = dp.Property()
    properties.dipole_gradients()
    assert 'dipole_gradients' in properties.settings
    assert properties.settings['dipole_gradients']


def test_polarizability_gradients():
    properties = dp.Property(polarizability_gradients=True)
    assert 'polarizability_gradients' in properties.settings
    assert properties.settings['polarizability_gradients_frequencies'] == [0.0]
    properties.polarizability_gradients(frequencies=[0.0, 0.1])
    assert properties.settings['polarizability_gradients']
    assert properties.settings['polarizability_gradients_frequencies'] == [0.0, 0.1]


def test_property_exceptions():
    properties = dp.Property()
    with pytest.raises(TypeError, match='Gradient type must be given as a string'):
        properties.gradients(method=['analytic'])
    with pytest.raises(TypeError, match='Hessian type must be given as a string'):
        properties.hessian(method=2)
    with pytest.raises(TypeError, match='Optimization method must be given as a string'):
        properties.geometry_optimization(method={'BFGS': True})
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.excitation_energies(states=5.0)
    with pytest.raises(TypeError, match='Triplet can be either True or False'):
        properties.excitation_energies(triplet=1)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states='5')
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states=5.0)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states=None)
    with pytest.raises(
            TypeError,
            match='Number of states must be given as an integer or list of integers if molecular symmetry is used'):
        properties.two_photon_absorption(states={5: 5})
    with pytest.raises(TypeError, match='Frequencies must be given as a float or a list of floats.'):
        properties.polarizability_gradients(frequencies=1)
    properties.excitation_energies(triplet=True)
    with pytest.raises(TypeError, match='Triplet excitations cannot be combined with two-photon absorption.'):
        properties.two_photon_absorption()
    properties.excitation_energies(triplet=False)
    properties.two_photon_absorption()
    with pytest.raises(TypeError, match='Triplet excitations cannot be combined with two-photon absorption.'):
        properties.excitation_energies(triplet=True)
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(gradients=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(hessian=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(geometry_optimization=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(excitation_energies=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(two_photon_absorption=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(hyperfine_couplings=['test'])
    with pytest.raises(TypeError, match='Wrong type:'):
        dp.Property(polarizability_gradients=['test'])
    with pytest.raises(TypeError, match='Core-valence separation'):
        prop = dp.Property()
        prop.excitation_energies(states=5, cvseparation=[1.2, 3, 4])
    with pytest.raises(TypeError, match='Core-valence separation'):
        prop = dp.Property()
        prop.excitation_energies(states=5, cvseparation=5)


def test_property_repr():
    properties = dp.Property(
        energy=True,
        gradients=True,
        hessian=True,
        geometry_optimization=True,
        transition_state=True,
        excitation_energies=True,
    )
    ref_repr = """Property(energy: True,
         gradients: analytic,
         hessian: analytic,
         geometry_optimization: BFGS,
         transition_state: True,
         excitation_energies: [5])"""
    assert repr(properties) == ref_repr
    properties = dp.Property(energy=True)
    ref_repr = 'Property(energy: True)'
    assert repr(properties) == ref_repr
    properties = dp.Property()
    ref_repr = 'Property()'
    assert repr(properties) == ref_repr
