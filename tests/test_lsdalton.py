import shutil
import textwrap
from pathlib import Path

import numpy as np
import pytest

import daltonproject as dp
from daltonproject.utilities import chdir


@pytest.mark.datafiles(pytest.DATADIR / 'water.xyz', )
def test_compute_exceptions(datafiles):
    hf = dp.QCMethod('HF')
    basis = dp.Basis(basis='3-21G')
    energy = dp.Property(energy=True)
    water = dp.Molecule(input_file=(datafiles / 'water.xyz'))
    with pytest.raises(TypeError):
        dp.lsdalton.compute(water, basis, basis, energy)
    with pytest.raises(TypeError):
        dp.lsdalton.compute(water, hf, hf, energy)
    with pytest.raises(TypeError):
        dp.lsdalton.compute(hf, basis, hf, energy)
    with pytest.raises(TypeError):
        dp.lsdalton.compute(water, basis, hf, hf)
    if not shutil.which(cmd='lsdalton'):
        with pytest.raises(FileNotFoundError, match='lsdalton script was not found or is not executable.'):
            dp.lsdalton.compute(water, basis, hf, energy)


@pytest.mark.datafiles(pytest.DATADIR / 'lsdalton_energy.out', )
def test_energy(datafiles):
    output = dp.lsdalton.OutputParser(datafiles / 'lsdalton_energy')
    reference = -76.072266941265
    assert abs(output.energy - reference) < 1e-11


@pytest.mark.datafiles(pytest.DATADIR / 'empty_file.out', )
def test_energy_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(Exception, match='Energy not found in'):
        output.energy


@pytest.mark.datafiles(pytest.DATADIR / 'lsdalton_energy.out', )
def test_electronic_energy(datafiles):
    output = dp.lsdalton.OutputParser(datafiles / 'lsdalton_energy')
    reference = -85.891743402431
    assert abs(output.electronic_energy - reference) < 1e-11


@pytest.mark.datafiles(pytest.DATADIR / 'empty_file.out', )
def test_electronic_energy_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(Exception, match='Electronic energy not found in'):
        output.electronic_energy


@pytest.mark.datafiles(pytest.DATADIR / 'lsdalton_energy.out', )
def test_nuclear_repulsion_energy(datafiles):
    output = dp.lsdalton.OutputParser(datafiles / 'lsdalton_energy')
    reference = 9.819476461165
    assert abs(output.nuclear_repulsion_energy - reference) < 1e-11


@pytest.mark.datafiles(pytest.DATADIR / 'empty_file.out', )
def test_nuclear_repulsion_energy_exceptions(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(Exception, match='Nuclear repulsion energy not found in'):
        output.nuclear_repulsion_energy


@pytest.mark.datafiles(pytest.DATADIR / 'empty_file.out', )
def test_notimplementederror(datafiles):
    filename = (datafiles / 'empty_file')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(NotImplementedError):
        output.dipole
    with pytest.raises(NotImplementedError):
        output.polarizabilities
    with pytest.raises(NotImplementedError):
        output.first_hyperpolarizability
    with pytest.raises(NotImplementedError):
        output.num_electrons
    with pytest.raises(NotImplementedError):
        output.num_orbitals
    with pytest.raises(NotImplementedError):
        output.num_basis_functions
    with pytest.raises(NotImplementedError):
        output.mo_energies
    with pytest.raises(NotImplementedError):
        output.homo_energy
    with pytest.raises(NotImplementedError):
        output.lumo_energy
    with pytest.raises(NotImplementedError):
        output.orbital_coefficients
    with pytest.raises(NotImplementedError):
        output.natural_occupations
    with pytest.raises(NotImplementedError):
        output.two_photon_tensors
    with pytest.raises(NotImplementedError):
        output.two_photon_strengths
    with pytest.raises(NotImplementedError):
        output.two_photon_cross_sections
    with pytest.raises(NotImplementedError):
        output.hyperfine_couplings
    with pytest.raises(NotImplementedError):
        output.spin_spin_couplings


@pytest.mark.datafiles(pytest.DATADIR / 'He.xyz', )
def test_no_rerun(datafiles):
    """Testing that a calculation will not rerun if the output file exist."""
    with chdir(datafiles):
        filename = '291300b17d65e33c56fa219fc9805722757ff3f1'
        with Path(f'{filename}.out').open('w') as f:
            f.write('\n@    Final HF energy:              -2.855160477243\n')
        with Path(f'{filename}.tar.gz').open('w') as f:
            f.write('\n\n')
        output = dp.lsdalton.compute(dp.Molecule(input_file=(datafiles / 'He.xyz')), dp.Basis(basis='3-21G'),
                                     dp.QCMethod('HF'), dp.Property(energy=True))
    assert output.energy == pytest.approx(-2.855160477243)


@pytest.mark.datafiles(pytest.DATADIR / 'molgrad_lsdalton.dal', )
def test_molecular_gradients_input(datafiles):
    with open((datafiles / 'molgrad_lsdalton.dal'), 'r') as file:
        reference = file.read()
    input_file = dp.lsdalton.program.lsdalton_input(dp.QCMethod('HF'), dp.Property(gradients=True))
    assert input_file == reference


@pytest.mark.datafiles(pytest.DATADIR / 'molgrad_lsdalton.out', )
def test_molecular_gradients(datafiles):
    reference = np.array([[0.0000003277, 0.0, 0.0], [-0.0000137883, 0.0, -0.0000054546],
                          [-0.0000137883, 0.0, 0.0000054546]])
    output = dp.lsdalton.OutputParser(datafiles / 'molgrad_lsdalton')
    np.testing.assert_allclose(output.gradients, reference)


@pytest.mark.datafiles(
    pytest.DATADIR / 'molgrad_trigger_no_num_atoms_lsdalton.out',
    pytest.DATADIR / 'molgrad_trigger_no_molgrad_lsdalton.out',
)
def test_molecular_gradients_exceptions(datafiles):
    filename = (datafiles / 'molgrad_trigger_no_num_atoms_lsdalton')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(Exception, match='Number of atoms not found in'):
        output.gradients
    filename = (datafiles / 'molgrad_trigger_no_molgrad_lsdalton')
    output = dp.lsdalton.OutputParser(filename)
    with pytest.raises(Exception, match='Molecular gradients not found in'):
        output.gradients


@pytest.mark.datafiles(pytest.DATADIR / 'water_ir.rsp_tensor', )
def test_hessian(datafiles):
    # yapf: disable
    ref_hessian = np.array(
        [[8.71164938e-01, -1.53164441e-17, -1.30487694e-14,
          -4.35587861e-01, 1.72934648e-16, -3.71076154e-01,
          -4.35587861e-01, -1.44508870e-16, 3.71076154e-01],
         [-1.53164441e-17, -5.20335224e-02, -2.83682813e-16,
          2.74191196e-16, 2.60147328e-02, 1.60416876e-16,
          -2.29491684e-16, 2.60147328e-02, 1.22649622e-16],
         [-1.30487694e-14, -2.83682813e-16, 4.64868108e-01,
          -3.88785554e-01, 7.89834553e-17, -2.32432909e-01,
          3.88785554e-01, 6.34538930e-17, -2.32432909e-01],
         [-4.35587861e-01, 2.74191196e-16, -3.88785554e-01,
          4.31516892e-01, -2.16369745e-16, 3.79932922e-01,
          4.07096860e-03, -3.46851627e-17, 8.85676835e-03],
         [1.72934648e-16, 2.60147328e-02, 7.89834553e-17,
          -2.16369745e-16, -9.17155783e-02, -2.57321090e-16,
          8.94423292e-17, 6.57008455e-02, 9.92106253e-17],
         [-3.71076154e-01, 1.60416876e-16, -2.32432909e-01,
          3.79932922e-01, -2.57321090e-16, 3.62865515e-01,
          -8.85325848e-03, 8.17972247e-17, -1.30432606e-01],
         [-4.35587861e-01, -2.29491684e-16, 3.88785554e-01,
          4.07096860e-03, 8.94423292e-17, -8.85325848e-03,
          4.31516892e-01, 1.79194033e-16, -3.79932922e-01],
         [-1.44508870e-16, 2.60147328e-02, 6.34538930e-17,
          -3.46851627e-17, 6.57008455e-02, 8.17972247e-17,
          1.79194033e-16, -9.17155783e-02, -2.21860247e-16],
         [3.71076154e-01, 1.22649622e-16, -2.32432909e-01,
          8.85676835e-03, 9.92106253e-17, -1.30432606e-01,
          -3.79932922e-01, -2.21860247e-16, 3.62865515e-01]]
    )
    # yapf: enable
    output = dp.lsdalton.OutputParser(datafiles / 'water_ir')
    np.testing.assert_allclose(output.hessian, np.array(ref_hessian))


@pytest.mark.datafiles(pytest.DATADIR / 'water_ir.rsp_tensor', )
def test_dipole_gradients(datafiles):
    # yapf: disable
    ref_dipole_gradients = np.array(
        [[2.86535262e-01, -1.37685537e-17, -1.82183033e-14],
         [-1.00931292e-16, 8.16171938e-01, 1.00545354e-15],
         [-2.21513346e-15, 2.95090984e-16, 4.82979597e-01],
         [-1.43267631e-01, -1.76845575e-16, 2.50612338e-01],
         [-8.18731871e-16, -4.08085969e-01, -4.03113906e-16],
         [2.27409153e-02, -1.64102083e-16, -2.41489798e-01],
         [-1.43267631e-01, 1.90614129e-16, -2.50612338e-01],
         [9.19663163e-16, -4.08085969e-01, -6.02339639e-16],
         [-2.27409153e-02, -1.30988901e-16, -2.41489798e-01]]
    )
    # yapf: enable
    output = dp.lsdalton.OutputParser(datafiles / 'water_ir')
    dipole_gradients = output.dipole_gradients
    np.testing.assert_allclose(dipole_gradients, ref_dipole_gradients)


@pytest.mark.datafiles(pytest.DATADIR / 'water_raman.rsp_tensor', )
def test_polarizability_gradients(datafiles):
    # yapf: disable
    ref_polarizability_gradients = np.array(
        [[[[8.20634980e+00, 1.71264330e-15, 1.64344953e-12],
           [1.71264330e-15, -7.55154922e-01, -6.10288536e-16],
           [1.64344953e-12, -6.10288536e-16, 5.08111144e+00]],
          [[-2.06713456e-16, 2.80078482e+00, -1.93159477e-15],
           [2.80078482e+00, 1.23313564e-15, -1.34579472e-13],
           [-1.93159477e-15, -1.34579472e-13, 5.17882501e-16]],
          [[1.63678478e-14, -1.13407011e-15, 5.02233373e+00],
           [-1.13407011e-15, -2.92821323e-15, -3.41065410e-17],
           [5.02233373e+00, -3.41065410e-17, -3.47975500e-13]],
          [[-4.10317490e+00, -3.75777731e-16, -3.20877053e+00],
           [-3.75777731e-16, 3.77577461e-01, 1.11776796e-15],
           [-3.20877053e+00, 1.11776796e-15, -2.54055572e+00]],
          [[8.03971305e-16, -1.40039241e+00, 7.14878604e-16],
           [-1.40039241e+00, -3.79773252e-16, -1.53782898e+00],
           [7.14878604e-16, -1.53782898e+00, 1.07138101e-15]],
          [[-1.05611737e+00, 2.63614522e-16, -2.51116687e+00],
           [2.63614522e-16, 2.55310064e-01, 5.75983161e-16],
           [-2.51116687e+00, 5.75983161e-16, -2.72151589e+00]],
          [[-4.10317490e+00, -1.33686557e-15, 3.20877053e+00],
           [-1.33686557e-15, 3.77577461e-01, -5.07479420e-16],
           [3.20877053e+00, -5.07479420e-16, -2.54055572e+00]],
          [[-5.97257849e-16, -1.40039241e+00, 1.21671617e-15],
           [-1.40039241e+00, -8.53362392e-16, 1.53782898e+00],
           [1.21671617e-15, 1.53782898e+00, -1.58926351e-15]],
          [[1.05611737e+00, 8.70455588e-16, -2.51116687e+00],
           [8.70455588e-16, -2.55310064e-01, -5.41876620e-16],
           [-2.51116687e+00, -5.41876620e-16, 2.72151589e+00]]],
         [[[8.49101765e+00, 1.70576920e-15, 1.07901819e-11],
           [1.70576920e-15, -7.71411944e-01, -6.16707408e-16],
           [1.00409693e-11, -6.16707408e-16, 5.48561812e+00]],
          [[-2.88058487e-16, 2.85876198e+00, -1.93903532e-15],
           [2.85876198e+00, 1.97747953e-15, 4.67268151e-13],
           [-1.93903532e-15, 4.59955424e-13, 5.23168929e-16]],
          [[-4.26254084e-15, -1.29282192e-15, 5.23829202e+00],
           [-1.29282192e-15, 6.51236877e-15, 2.94573265e-16],
           [5.23829202e+00, 2.94573265e-16, 1.05053745e-12]],
          [[-4.24550882e+00, -2.61044698e-16, -3.38176603e+00],
           [-2.61044698e-16, 3.85705972e-01, 1.16598371e-15],
           [-3.38176603e+00, 1.16598371e-15, -2.74280906e+00]],
          [[8.27126977e-16, -1.42938099e+00, 7.33782329e-16],
           [-1.42938099e+00, -6.64467327e-16, -1.59199304e+00],
           [7.33782329e-16, -1.59199304e+00, 1.06397630e-15]],
          [[-1.13693828e+00, 4.43620474e-16, -2.61914601e+00],
           [4.43620474e-16, 2.63502025e-01, 3.86819760e-16],
           [-2.61914601e+00, 3.86819760e-16, -2.83488382e+00]],
          [[-4.24550882e+00, -1.44472450e-15, 3.38176603e+00],
           [-1.44472450e-15, 3.85705972e-01, -5.49276305e-16],
           [3.38176603e+00, -5.49276305e-16, -2.74280906e+00]],
          [[-5.39068490e-16, -1.42938099e+00, 1.20525300e-15],
           [-1.42938099e+00, -1.31301220e-15, 1.59199304e+00],
           [1.20525300e-15, 1.59199304e+00, -1.58714523e-15]],
          [[1.13693828e+00, 8.49201448e-16, -2.61914601e+00],
           [8.49201448e-16, -2.63502025e-01, -6.81393024e-16],
           [-2.61914601e+00, -6.81393024e-16, 2.83488382e+00]]]]
    )
    # yapf: enable
    result = dp.lsdalton.OutputParser(datafiles / 'water_raman')
    np.testing.assert_allclose(result.polarizability_gradients.values[0],
                               ref_polarizability_gradients[0],
                               atol=1e-8)
    assert result.polarizability_gradients.frequencies[0] == 0.0
    np.testing.assert_allclose(result.polarizability_gradients.values[1],
                               ref_polarizability_gradients[1],
                               atol=1e-8)
    assert result.polarizability_gradients.frequencies[1] == 0.1


@pytest.mark.parametrize(
    'method, properties, expected',
    [
        (
            {'qc_method': 'HF', 'scf_threshold': 1e-6},
            {'energy': True},
            textwrap.dedent("""\
                **GENERAL
                .NOGCBASIS
                .TIME
                **WAVE FUNCTION
                .HF
                *DENSOPT
                .VanLenthe
                .CONVTHR
                1e-06
                **INTEGRALS
                .NOFAMILY
                *END OF INPUT
            """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'B3LYP'},
            {'geometry_optimization': True},
            textwrap.dedent("""\
                **GENERAL
                .NOGCBASIS
                .TIME
                **WAVE FUNCTION
                .DFT
                B3LYP
                *DENSOPT
                .VanLenthe
                .RESTART
                **INTEGRALS
                .NOFAMILY
                **OPTIMIZE
                .NOHOPE
                *END OF INPUT
            """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'B3LYP'},
            {'excitation_energies': {'states': 10}},
            textwrap.dedent("""\
                **GENERAL
                .NOGCBASIS
                .TIME
                **WAVE FUNCTION
                .DFT
                B3LYP
                *DENSOPT
                .VanLenthe
                **INTEGRALS
                .NOFAMILY
                **RESPONS
                .NEXCIT
                10
                *END OF INPUT
            """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'CAM-B3LYP', 'coulomb': 'DF', 'exchange': 'ADMM'},
            {'gradients': True},
            textwrap.dedent("""\
                **GENERAL
                .NOGCBASIS
                .TIME
                **WAVE FUNCTION
                .DFT
                CAM-B3LYP
                *DENSOPT
                .VanLenthe
                **INTEGRALS
                .DENSFIT
                .ADMM
                .NOFAMILY
                **RESPONS
                *MOLGRA
                *END OF INPUT
            """),
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'PBE0'},
            {'hessian': True, 'dipole_gradients': True, 'polarizability_gradients': {'frequencies': [0.0, 0.1]}},
            textwrap.dedent("""\
                **GENERAL
                .NOGCBASIS
                .TIME
                **WAVE FUNCTION
                .DFT
                PBE0
                *DENSOPT
                .VanLenthe
                **INTEGRALS
                .NOFAMILY
                **OPENRSP
                *RESPONSE FUNCTION
                $PROPERTY
                .PERTURBATION
                2
                GEO
                GEO
                .KN-RULE
                0
                $PROPERTY
                .PERTURBATION
                2
                GEO
                EL
                .KN-RULE
                0
                $PROPERTY
                .PERTURBATION
                3
                GEO
                EL
                EL
                .FREQUENCY
                2
                0.0
                0.1
                .KN-RULE
                0
                *END OF INPUT
            """),
        ),
    ],
    ids=[
        'hf-energy', 'dft-geometry_optimization', 'dft-excitation_energies', 'dft-jkfit-gradients', 'dft-openrsp'
    ],
)
def test_lsdalton_input(method, properties, expected):
    """Testing what will be written to the dal file."""
    generated = dp.lsdalton.program.lsdalton_input(dp.qcmethod.QCMethod(**method),
                                                   dp.property.Property(**properties))
    assert generated == expected


@pytest.mark.parametrize(
    'method, properties, exception, message',
    [
        (
            {'qc_method': 'DFT', 'xc_functional': 'B3LYP', 'exact_exchange': 0.4},
            {'energy': True},
            NotImplementedError,
            'Exact exchange cannot be changed for LSDalton through Dalton Project.',
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'B3LYP'},
            {'excitation_energies': {'states': [3, 4]}},
            ValueError,
            'LSDalton does not support molecular symmetry. Number of states must be an integer.',
        ),
        (
            {'qc_method': 'DFT', 'xc_functional': 'CAM-B3LYP', 'coulomb': 'DF', 'exchange': 'ADMM'},
            {'two_photon_absorption': True},
            NotImplementedError,
            'Two-photon absorption not available for LSDalton.',
        ),
    ],
    ids=['exact_exchange', 'symmetry', 'two_photon_absorption'],
)
def test_lsdalton_input_exceptions(method, properties, exception, message):
    """Testing what will be written to the dal file."""
    with pytest.raises(exception, match=message):
        dp.lsdalton.program.lsdalton_input(dp.qcmethod.QCMethod(**method), dp.property.Property(**properties))


def test_dft_exceptions(datafiles):
    qcmethod = dp.QCMethod('DFT', 'PBE')
    qcmethod.exact_exchange(0.2)
    with pytest.raises(NotImplementedError, match='Exact exchange cannot be changed for'):
        dp.lsdalton.program.lsdalton_input(qcmethod, dp.Property(energy=True))
