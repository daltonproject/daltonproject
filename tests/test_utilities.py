import os
import socket
import sys

import parse
import pytest

import daltonproject as dp
from daltonproject import utilities


def test_chdir(tmpdir):
    old_cwd = os.getcwd()
    assert old_cwd != tmpdir
    with utilities.chdir(tmpdir):
        current_cwd = os.getcwd()
        assert current_cwd == tmpdir
        assert current_cwd != old_cwd
    assert old_cwd == os.getcwd()
    with utilities.chdir(tmpdir):
        current_cwd = os.getcwd()
        assert current_cwd == tmpdir
        assert current_cwd != old_cwd
        with pytest.raises(Exception):
            raise Exception
    assert old_cwd == os.getcwd()


def test_unique_filename():
    str_1 = 'dal string'
    str_2 = 'mol string'
    str_3 = 'pot string'
    str_4 = 'other string'
    filename = utilities.unique_filename([str_1, str_2, str_3, str_4])
    assert filename == 'fc1feaad96f2e81497a9dd79d4044531e4c9a57e'


def test_unique_filename_exceptions():
    str_1 = 'dal string'
    with pytest.raises(TypeError, match='Expected Sequence, str found'):
        utilities.unique_filename(str_1)
    str_1 = 'dal string'
    list_2 = ['mol string']
    with pytest.raises(TypeError, match='Sequence item 1: expected str instance, list found'):
        utilities.unique_filename([str_1, list_2])
    str_1 = 'dal string'
    str_2 = 'mol string'
    int_3 = 666
    with pytest.raises(TypeError, match='Sequence item 2: expected str instance, int found'):
        utilities.unique_filename([str_1, str_2, int_3])


def test_num_numa_nodes():
    assert isinstance(utilities.num_numa_nodes(), int)


def test_num_cpu_cores():
    assert isinstance(utilities.num_cpu_cores(), int)


def test_run():
    command = f'{sys.executable} --version'
    stdout, stderr, return_code = utilities.run(command)
    assert 'Python 3.' in stdout
    assert stderr == ''
    assert return_code == 0


def test_run_exceptions():
    with pytest.raises(TypeError, match='Expected str, list found'):
        utilities.run(['test'])
    with pytest.raises(TypeError, match='Expected str, int found'):
        utilities.run(666)
    with pytest.raises(TypeError, match='Expected str, float found'):
        utilities.run(0.0)


def test_job_farm(tmpdir):
    with utilities.chdir(tmpdir):
        settings = dp.ComputeSettings()
        results = utilities.job_farm(commands=[f'{sys.executable} --version'],
                                     node_list=settings.node_list,
                                     jobs_per_node=1,
                                     comm_port=settings.comm_port)
    stdout, stderr, return_code = results[0]
    assert 'Python 3.' in stdout
    assert stderr == ''
    assert return_code == 0


def test_create_server_manager():
    manager = utilities.create_server_manager(comm_port=666, auth_key=b'666')
    assert manager.address == ('', 666)


def test_create_client_script(tmpdir):
    script_name = 'test.py'
    jobs_per_node = 666
    comm_port = 666
    auth_key = b'666'
    with utilities.chdir(tmpdir):
        utilities.create_client_script(script_name=script_name,
                                       jobs_per_node=jobs_per_node,
                                       comm_port=comm_port,
                                       auth_key=auth_key)
        assert os.path.isfile(script_name)
        with open(script_name) as script_file:
            client_script = script_file.read()
        result = parse.search('for i in range({jobs_per_node}):', client_script)
        assert int(result.named['jobs_per_node']) == jobs_per_node
        result = parse.search('address=(\'{hostname}\', {comm_port}), authkey={auth_key})', client_script)
        assert result.named['hostname'] == socket.gethostname()
        assert int(result.named['comm_port']) == comm_port
        assert result.named['auth_key'] == str(auth_key)


def test_destroy_client(tmpdir):
    script_name = 'test.py'
    jobs_per_node = 666
    comm_port = 666
    auth_key = b'666'
    with utilities.chdir(tmpdir):
        utilities.create_client_script(script_name=script_name,
                                       jobs_per_node=jobs_per_node,
                                       comm_port=comm_port,
                                       auth_key=auth_key)
        assert os.path.isfile(script_name)
        utilities.destroy_clients(script_name)
        assert not os.path.isfile(script_name)
