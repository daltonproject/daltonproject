import os
import runpy
from shutil import copyfile

from daltonproject.utilities import chdir


def test_casscf_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/pyridine.mol'), 'pyridine.mol')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/simple_cas.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2_no_inspection.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/casscf/mp2no_cas.py'))


def test_spectrum_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/water.xyz'), 'water.xyz')
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/acrolein.xyz'),
                 'acrolein.xyz')
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/H2O2.xyz'), 'H2O2.xyz')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/simple_tddft.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/simple_tpa.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/cvs_cc.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/spectrum/ir_raman.py'))


def test_srdft_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/water.mol'), 'water.mol')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/hfsrdft.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/lrcdft.py'))
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/pyridine.mol'), 'pyridine.mol')
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/mp2srdft.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__),
                                    '../docs/tutorials/srdft/mp2srdft_no_inspection.py'))
        runpy.run_path(os.path.join(os.path.dirname(__file__), '../docs/tutorials/srdft/cassrdft.py'))


def test_vib_averaging_tutorial(tmpdir):
    with chdir(tmpdir):
        copyfile(os.path.join(os.path.dirname(__file__), '../docs/tutorials/vibrational_corrections/water.xyz'),
                 'water.xyz')
        runpy.run_path(
            os.path.join(os.path.dirname(__file__),
                         '../docs/tutorials/vibrational_corrections/shieldings_linear.py'))
        runpy.run_path(
            os.path.join(os.path.dirname(__file__),
                         '../docs/tutorials/vibrational_corrections/sscc_polynomial.py'))
        runpy.run_path(
            os.path.join(os.path.dirname(__file__),
                         '../docs/tutorials/vibrational_corrections/freq_dependent_polar.py'))
